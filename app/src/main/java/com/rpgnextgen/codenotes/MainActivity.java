package com.rpgnextgen.codenotes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.AlignContent;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import com.rpgnextgen.codenotes.file.CreateFileDialogActivity;
import com.rpgnextgen.codenotes.file.EditFileDialogActivity;
import com.rpgnextgen.codenotes.file.FileActivity;
import com.rpgnextgen.codenotes.file.FileConnectionProvider;
import com.rpgnextgen.codenotes.git.CreateGitDialogActivity;
import com.rpgnextgen.codenotes.git.EditGitDialogActivity;
import com.rpgnextgen.codenotes.git.GitActivity;
import com.rpgnextgen.codenotes.git.GitConnectionProvider;
import com.rpgnextgen.codenotes.ibmi.ifs.CreateIfsConnectionDialogActivity;
import com.rpgnextgen.codenotes.ibmi.ifs.EditIfsConnectionDialogActivity;
import com.rpgnextgen.codenotes.ibmi.ifs.IfsActivity;
import com.rpgnextgen.codenotes.ibmi.ifs.IfsConnectionProvider;
import com.rpgnextgen.codenotes.ibmi.qsys.CreateIbmiConnectionDialogActivity;
import com.rpgnextgen.codenotes.ibmi.qsys.EditIbmiConnectionDialogActivity;
import com.rpgnextgen.codenotes.ibmi.qsys.QsysActivity;
import com.rpgnextgen.codenotes.ibmi.qsys.QsysConnectionProvider;
import com.rpgnextgen.codenotes.offline.OfflineActivity;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnLongClickListener {

    private enum Mode {
        online, offline
    }

    public static final int REQUEST_CODE_CREATE_CONFIG = 1;
    public static final int REQUEST_CODE_EDIT_CONFIG = 2;

    public static final String CACHE_MODE = "cache.mode";

    private Dialog fabDialog;
    private Mode mode = Mode.online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            LayoutInflater inflater = MainActivity.this.getLayoutInflater();
            View layout = inflater.inflate(R.layout.dialog_connection_type, null);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Choose Source Type");
            builder.setView(layout);

            AlertDialog dialog = builder.create();
            MainActivity.this.fabDialog = dialog;
            dialog.show();
        });

        loadConfigurations();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        if (savedInstanceState != null) {
            mode = Mode.valueOf(savedInstanceState.getString(CACHE_MODE));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SwitchCompat offlineSwitch = (SwitchCompat) menu.findItem(R.id.action_offline).getActionView();
        offlineSwitch.setText("Mode:");
        offlineSwitch.setChecked(mode == Mode.online);
        offlineSwitch.setShowText(true);
        offlineSwitch.setTextOff("offline");
        offlineSwitch.setTextOn("online");
        offlineSwitch.setOnCheckedChangeListener((button, value) -> {
            mode = value ? Mode.online : Mode.offline;
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void selectSourceType(View view) {
        if (view.getTag().equals(QsysConnectionProvider.ID)) {
            Intent intent = new Intent(this, CreateIbmiConnectionDialogActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CREATE_CONFIG);
        }
        else if (view.getTag().equals(IfsConnectionProvider.ID)) {
            Intent intent = new Intent(this, CreateIfsConnectionDialogActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CREATE_CONFIG);
        }
        else if (view.getTag().equals(GitConnectionProvider.ID)) {
            Intent intent = new Intent(this, CreateGitDialogActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CREATE_CONFIG);
        }
        else if (view.getTag().equals(FileConnectionProvider.ID)) {
            Intent intent = new Intent(this, CreateFileDialogActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CREATE_CONFIG);
        }

        fabDialog.dismiss();
        fabDialog = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_MODE, mode.name());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CREATE_CONFIG && resultCode == RESULT_OK) {
            loadConfigurations();
        }
        else if (requestCode == REQUEST_CODE_EDIT_CONFIG) {
            loadConfigurations();
        }
    }

    @Override
    public boolean onLongClick(View view) {
        Map<String, ?> taggedData = (Map<String, ?>) view.getTag();

        if (taggedData.get(ConnectionService.PREF_TYPE).equals(QsysConnectionProvider.ID)) {
            Intent intent = new Intent(this, EditIbmiConnectionDialogActivity.class);
            intent.putExtra(ConnectionService.PREF_ID, taggedData.get(ConnectionService.PREF_ID).toString());
            startActivityForResult(intent, REQUEST_CODE_EDIT_CONFIG);
        }
        else if (taggedData.get(ConnectionService.PREF_TYPE).equals(IfsConnectionProvider.ID)) {
            Intent intent = new Intent(this, EditIfsConnectionDialogActivity.class);
            intent.putExtra(ConnectionService.PREF_ID, taggedData.get(ConnectionService.PREF_ID).toString());
            startActivityForResult(intent, REQUEST_CODE_EDIT_CONFIG);
        }
        else if (taggedData.get(ConnectionService.PREF_TYPE).equals(GitConnectionProvider.ID)) {
            Intent intent = new Intent(this, EditGitDialogActivity.class);
            intent.putExtra(ConnectionService.PREF_ID, taggedData.get(ConnectionService.PREF_ID).toString());
            startActivityForResult(intent, REQUEST_CODE_EDIT_CONFIG);
        }
        else if (taggedData.get(ConnectionService.PREF_TYPE).equals(FileConnectionProvider.ID)) {
            Intent intent = new Intent(this, EditFileDialogActivity.class);
            intent.putExtra(ConnectionService.PREF_ID, taggedData.get(ConnectionService.PREF_ID).toString());
            startActivityForResult(intent, REQUEST_CODE_EDIT_CONFIG);
        }

        return true;
    }

    public void connect(View view) {
        Map<String, ?> configuration = (Map<String, ?>) view.getTag();

        if (mode == Mode.offline) {
            Intent intent = new Intent(this, OfflineActivity.class);
            intent.putExtra(ConnectionService.PREF_ID, configuration.get(ConnectionService.PREF_ID).toString());
            startActivity(intent);
        }
        else {
            if (configuration.get(ConnectionService.PREF_TYPE).equals(QsysConnectionProvider.ID)) {
                Intent intent = new Intent(this, QsysActivity.class);
                intent.putExtra(ConnectionService.PREF_ID, configuration.get(ConnectionService.PREF_ID).toString());
                startActivity(intent);
            }
            else if (configuration.get(ConnectionService.PREF_TYPE).equals(IfsConnectionProvider.ID)) {
                Intent intent = new Intent(this, IfsActivity.class);
                intent.putExtra(ConnectionService.PREF_ID, configuration.get(ConnectionService.PREF_ID).toString());
                startActivity(intent);
            }
            else if (configuration.get(ConnectionService.PREF_TYPE).equals(GitConnectionProvider.ID)) {
                Intent intent = new Intent(this, GitActivity.class);
                intent.putExtra(ConnectionService.PREF_ID, configuration.get(ConnectionService.PREF_ID).toString());
                startActivity(intent);
            }
            else if (configuration.get(ConnectionService.PREF_TYPE).equals(FileConnectionProvider.ID)) {
                Intent intent = new Intent(this, FileActivity.class);
                intent.putExtra(ConnectionService.PREF_ID, configuration.get(ConnectionService.PREF_ID).toString());
                startActivity(intent);
            }
        }
    }

    private void loadConfigurations() {
        List<String> configurations =  ConnectionService.getGlobalConfigurations(this);
        Collections.sort(configurations);

        if (configurations.isEmpty()) {
            ViewGroup contentLayout = findViewById(R.id.main_content_layout);
            contentLayout.removeAllViews();
            View empty = getLayoutInflater().inflate(R.layout.content_main_empty, null);

            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(-1, -1);
            layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
            empty.setLayoutParams(layoutParams);

            contentLayout.addView(empty);
        }
        else {
            ViewGroup contentLayout = findViewById(R.id.main_content_layout);
            contentLayout.removeAllViews();

            FlexboxLayout connectionLayout = new FlexboxLayout(this);
            connectionLayout.setFlexDirection(FlexDirection.ROW);
            connectionLayout.setFlexWrap(FlexWrap.WRAP);
            connectionLayout.setAlignContent(AlignContent.STRETCH);
            connectionLayout.setAlignItems(AlignItems.STRETCH);

            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            connectionLayout.setLayoutParams(layoutParams);

            contentLayout.addView(connectionLayout);

            for (String configName : configurations) {
                Map<String, ?> configuration = ConnectionService.get(this, configName);

                ViewGroup connection = (ViewGroup) getLayoutInflater().inflate(R.layout.connection_template, null);
                TextView connectionText = connection.findViewById(R.id.connection_text);
                connectionText.setText(configuration.get("config.name").toString());
                connectionText.setGravity(Gravity.CENTER);
                connectionText.setTag(configuration);
                ImageView connectionIcon = connection.findViewById(R.id.connection_icon);
                connectionIcon.setTag(configuration);
                connectionIcon.setOnLongClickListener(this);

                Bitmap icon = getConnectionIcon(configuration.get(ConnectionService.PREF_TYPE).toString());
                connectionIcon.setImageBitmap(icon);

                connectionLayout.addView(connection);
            }

        }
    }

    private Bitmap getConnectionIcon(String connectionType) {
        if (connectionType.equals(QsysConnectionProvider.ID)) {
            return BitmapFactory.decodeResource(getResources(), R.drawable.ibmi);
        }
        else if (connectionType.equals(IfsConnectionProvider.ID)) {
            return BitmapFactory.decodeResource(getResources(), R.drawable.ibmi);
        }
        else if (connectionType.equals(GitConnectionProvider.ID)) {
            return BitmapFactory.decodeResource(getResources(), R.drawable.git);
        }
        else if (connectionType.equals(FileConnectionProvider.ID)) {
            return BitmapFactory.decodeResource(getResources(), R.drawable.zip_icon);
        }
        else
            return null;
    }

    public void openSettingsActivity(MenuItem item) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

}
