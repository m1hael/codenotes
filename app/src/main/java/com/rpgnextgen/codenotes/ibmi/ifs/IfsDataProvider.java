package com.rpgnextgen.codenotes.ibmi.ifs;

import android.util.Log;

import com.ibm.jtopenlite.file.FileHandle;
import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.endless.api.IPagedDataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IfsDataProvider implements IPagedDataProvider<FileHandle> {

    private List<FileHandle> cache = new ArrayList<>();
    private String server;
    private String user;
    private String pass;
    private String path;

    public IfsDataProvider(String server, String user, String pass, String path) {
        this.server = server;
        this.user = user;
        this.pass = pass;
        this.path = path;
    }

    @Override
    public List<FileHandle> getNextBlock() throws IOException {
        if (cache.isEmpty()) {
            load();
        }

        return cache;
    }

    @Override
    public void reset() {
        cache.clear();
    }

    @Override
    public void reload() throws IOException {
        cache.clear();
        load();
    }

    @Override
    public void setBlockSize(int size) {
        // ignore
    }

    @Override
    public boolean hasMore() {
        return false;
    }

    @Override
    public String getSearchTerm() {
        return null;
    }

    @Override
    public List<FileHandle> getCached() {
        return cache;
    }

    public void setCached(List<FileHandle> cache) {
        this.cache.clear();
        this.cache.addAll(cache);
    }

    public void setPath(String path) {
        this.path = path;
    }

    private void load() {
        try {
            cache.addAll(ConnectionService.loadFiles(server, user, pass, path));
            Collections.sort(cache);
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not load files for " + path, e);
        }
    }

}
