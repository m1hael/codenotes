package com.rpgnextgen.codenotes.ibmi.qsys;

import android.util.Log;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.endless.api.IPagedDataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ObjectDataProvider implements IPagedDataProvider<ObjectInfo> {

    private List<ObjectInfo> cache = new ArrayList<>();
    private String server;
    private String user;
    private String pass;
    private String library;

    public ObjectDataProvider(String server, String user, String pass, String library) {
        this.server = server;
        this.user = user;
        this.pass = pass;
        this.library = library;
    }

    @Override
    public List<ObjectInfo> getNextBlock() throws IOException {
        if (cache.isEmpty()) {
            load();
        }

        return cache;
    }

    @Override
    public void reset() {
        cache.clear();
    }

    @Override
    public void reload() throws IOException {
        cache.clear();
        load();
    }

    @Override
    public void setBlockSize(int size) {
        // ignore
    }

    @Override
    public boolean hasMore() {
        return false;
    }

    @Override
    public String getSearchTerm() {
        return null;
    }

    @Override
    public List<ObjectInfo> getCached() {
        return cache;
    }

    public void setCached(List<ObjectInfo> cache) {
        this.cache.clear();
        this.cache.addAll(cache);
    }

    private void load() {
        try {
            List<ObjectInfo> objects = new LinkedList<ObjectInfo>(ConnectionService.loadObjects(server, user, pass, library));
            ListIterator<ObjectInfo> iterator = objects.listIterator();
            while(iterator.hasNext()) {
                ObjectInfo objectInfo = iterator.next();
                if (objectInfo.getType().trim().equals("*FILE") && objectInfo.getAttribute().trim().equals("PF")) {
                    // ok, may be a source file
                }
                else {
                    iterator.remove();
                }
            }
            cache.addAll(objects);
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not load objects for library " + library, e);
        }
    }
}