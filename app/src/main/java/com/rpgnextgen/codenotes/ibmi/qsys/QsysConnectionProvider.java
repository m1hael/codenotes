package com.rpgnextgen.codenotes.ibmi.qsys;

import com.rpgnextgen.codenotes.R;

public class QsysConnectionProvider {

    public static final int LOGO_ID = R.drawable.ibmi;
    public static final String name = "IBM i Source Files";
    public static final String PREF_LIBRARIES = "config.ibmi.qsys.libraries";
    public static final String ID = QsysConnectionProvider.class.getPackage().getName();


}
