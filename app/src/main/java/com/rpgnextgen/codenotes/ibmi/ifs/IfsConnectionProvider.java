package com.rpgnextgen.codenotes.ibmi.ifs;

import com.rpgnextgen.codenotes.R;

public class IfsConnectionProvider {

    public static final int LOGO_ID = R.drawable.ibmi;
    public static final String name = "IBM i IFS Files";
    public static final String PREF_PATH = "config.ibmi.ifs.path";
    public static final String ID = IfsConnectionProvider.class.getPackage().getName();

}
