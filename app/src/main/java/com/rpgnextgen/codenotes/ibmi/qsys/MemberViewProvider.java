package com.rpgnextgen.codenotes.ibmi.qsys;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.endless.api.IEntryViewProvider;

public class MemberViewProvider implements IEntryViewProvider<MemberInfo> {

    private LayoutInflater inflater;

    public MemberViewProvider(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View get(MemberInfo entry, View view) {
        LinearLayout layout;

        if (view == null) {
            layout = (LinearLayout) inflater.inflate(R.layout.content_qsys_entry, null);
        } else if (view instanceof LinearLayout) {
            layout = (LinearLayout) view;
        } else {
            layout = (LinearLayout) inflater.inflate(R.layout.content_qsys_entry, null);
        }

        TextView nameView = layout.findViewById(R.id.qsys_entry_name);
        nameView.setText(entry.getName());
        nameView.setTag(entry);

        TextView typeView = layout.findViewById(R.id.qsys_entry_attribute);
        typeView.setText(entry.getType());
        typeView.setTag(entry);

        TextView descriptionView = layout.findViewById(R.id.qsys_entry_description);
        descriptionView.setTag(entry);
        if (entry.getDescription().isEmpty()) {
            descriptionView.setVisibility(View.GONE);
        }
        else {
            descriptionView.setVisibility(View.VISIBLE);
            descriptionView.setText(entry.getDescription());
        }

        Checkable selectionWidget = layout.findViewById(android.R.id.checkbox);
        selectionWidget.setChecked(entry.isChecked());

        layout.setTag(entry);

        return layout;
    }
}