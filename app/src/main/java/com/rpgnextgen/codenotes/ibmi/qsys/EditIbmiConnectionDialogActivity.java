package com.rpgnextgen.codenotes.ibmi.qsys;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EditIbmiConnectionDialogActivity extends AppCompatActivity {

    private String configId;

    private Set<String> libraries = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connection_ibmi);

        if (savedInstanceState != null) {
            configId = savedInstanceState.getString(ConnectionService.PREF_ID);

            EditText field = findViewById(R.id.ibmi_source_name);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_NAME, ""));
            // the name should not be changed (create a new connection if you want another name)
            field.setEnabled(false);

            field = findViewById(R.id.ibmi_source_server);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_SERVER, ""));

            field = findViewById(R.id.ibmi_source_user);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_USER, ""));

            field = findViewById(R.id.ibmi_source_password);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_PASSWORD, ""));

            String[] libraryNames = savedInstanceState.getString(QsysConnectionProvider.PREF_LIBRARIES, "").split(",");
            libraries.addAll(Arrays.asList(libraryNames));

            for (String libraryName : libraryNames) {
                if (!TextUtils.isEmpty(libraryName)) {
                    addLibraryLayout(libraryName);
                }
            }

        }
        else {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);

            Map<String, ?> config = ConnectionService.get(this, configId);
            EditText field = findViewById(R.id.ibmi_source_name);
            field.setText(config.get(ConnectionService.PREF_NAME).toString());
            // the name should not be changed (create a new connection if you want another name)
            field.setEnabled(false);

            field = findViewById(R.id.ibmi_source_server);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_SERVER)));

            field = findViewById(R.id.ibmi_source_user);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_USER)));

            field = findViewById(R.id.ibmi_source_password);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_PASSWORD)));

            String[] libraryNames = config.get(QsysConnectionProvider.PREF_LIBRARIES).toString().split(",");
            libraries.addAll(Arrays.asList(libraryNames));

            for (String libraryName : libraryNames) {
                if (!TextUtils.isEmpty(libraryName)) {
                    addLibraryLayout(libraryName);
                }
            }
        }
    }

    private String getDefaultString(Object o) {
        if (o == null) {
            return "";
        }

        return o.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ibmi_qsys_edit, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(ConnectionService.PREF_ID, configId);

        EditText field = findViewById(R.id.ibmi_source_name);
        outState.putString(ConnectionService.PREF_NAME, field.getText().toString());

        field = findViewById(R.id.ibmi_source_server);
        outState.putString(ConnectionService.PREF_SERVER, field.getText().toString());

        field = findViewById(R.id.ibmi_source_user);
        outState.putString(ConnectionService.PREF_USER, field.getText().toString());

        field = findViewById(R.id.ibmi_source_password);
        outState.putString(ConnectionService.PREF_PASSWORD, field.getText().toString());

        outState.putString(QsysConnectionProvider.PREF_LIBRARIES, TextUtils.join(",", libraries));
    }

    public void deleteConfiguration(MenuItem item) {
        ConnectionService.delete(this, configId);

        Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Configuration deleted", Snackbar.LENGTH_SHORT).show();

        setResult(RESULT_OK);
        finish();
    }

    public void saveConfiguration(MenuItem item) {
        String name = ((EditText) findViewById(R.id.ibmi_source_name)).getText().toString();
        String server = ((EditText) findViewById(R.id.ibmi_source_server)).getText().toString();
        String user = ((EditText) findViewById(R.id.ibmi_source_user)).getText().toString();
        String pass = ((EditText) findViewById(R.id.ibmi_source_password)).getText().toString();

        if (TextUtils.isEmpty(server)) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a server for the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (TextUtils.isEmpty(user)) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a username for the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (libraries.isEmpty()) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please add at least one library to the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else {
            String qualifiedConfigurationName = buildConfigurationName(name);

            // save the config
            Map<String,String> config = new HashMap<>();
            config.put(ConnectionService.PREF_ID, qualifiedConfigurationName);
            config.put(ConnectionService.PREF_NAME, name);
            config.put(ConnectionService.PREF_TYPE, QsysConnectionProvider.ID);
            config.put(ConnectionService.PREF_SERVER, server);
            config.put(ConnectionService.PREF_USER, user);
            config.put(ConnectionService.PREF_PASSWORD, pass);
            config.put(QsysConnectionProvider.PREF_LIBRARIES, TextUtils.join(",", libraries));
            ConnectionService.save(this, config);

            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Configuration saved.", Snackbar.LENGTH_SHORT).show();

            setResult(RESULT_OK);
            finish();
        }
    }

    @NonNull
    private String buildConfigurationName(String name) {
        return this.getClass().getPackage().getName() + "." + name;
    }

    public void addLibrary(MenuItem menuItem) {
        final EditText libraryField = new EditText(this);
        libraryField.setAllCaps(true);
        libraryField.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Add library")
                .setMessage("Enter a library name:")
                .setView(libraryField)
                .setPositiveButton("Add", (dialogInterface, whichButton) -> {
                    String libraryName = libraryField.getText().toString();
                    libraries.add(libraryName);

                    addLibraryLayout(libraryName);

                    dialogInterface.dismiss();
                })
                .setNegativeButton("Cancel", (dialogInterface, whichButton) -> dialogInterface.dismiss())
                .create();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dialog.show();
    }

    private void addLibraryLayout(String libraryName) {
        ViewGroup libraryLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.connection_ibmi_library, null);
        libraryLayout.setTag(libraryName);
        TextView libraryNameField = libraryLayout.findViewById(R.id.ibmi_library_name);
        libraryNameField.setText(libraryName);
        libraryLayout.findViewById(R.id.ibmi_library_remove).setTag(libraryName);

        ViewGroup librariesLayout = findViewById(R.id.ibmi_libraries_layout);
        librariesLayout.addView(libraryLayout);
    }

    public void removeLibrary(View view) {
        String libraryName = view.getTag().toString();

        ViewGroup librariesLayout = findViewById(R.id.ibmi_libraries_layout);
        int childCount = librariesLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = librariesLayout.getChildAt(i);
            if (libraryName.equals(childView.getTag())) {
                librariesLayout.removeViewAt(i);
                libraries.remove(libraryName);
                break;
            }
        }
    }
}
