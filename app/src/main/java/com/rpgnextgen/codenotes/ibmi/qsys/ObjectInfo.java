package com.rpgnextgen.codenotes.ibmi.qsys;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Checkable;

public class ObjectInfo implements Parcelable, Checkable {

    private boolean checked;
    private String name;
    private String library;
    private String type;
    private String attribute;
    private String description;

    public ObjectInfo(String name, String library, String type, String attribute, String description) {
        this.name = name;
        this.library = library;
        this.type = type;
        this.attribute = attribute;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getLibrary() {
        return library;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getAttribute() {
        return attribute;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeString(library);
        parcel.writeString(type);
        parcel.writeString(attribute);
        parcel.writeString(description);
    }

    public static final Parcelable.Creator<ObjectInfo> CREATOR = new Parcelable.Creator<ObjectInfo>() {

        @Override
        public ObjectInfo createFromParcel(Parcel parcel) {
            ObjectInfo objectInfo = new ObjectInfo(
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString()
            );
            return objectInfo;
        }

        @Override
        public ObjectInfo[] newArray(int size) {
            return new ObjectInfo[size];
        }
    };

    @Override
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public void toggle() {
        checked = !checked;
    }
}
