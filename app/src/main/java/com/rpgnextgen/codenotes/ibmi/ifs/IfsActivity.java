package com.rpgnextgen.codenotes.ibmi.ifs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.ibm.jtopenlite.file.FileConnection;
import com.ibm.jtopenlite.file.FileHandle;
import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.offline.OfflineService;
import com.rpgnextgen.codenotes.OutlineProviderSelector;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.SourceCodeActivity;
import com.rpgnextgen.endless.api.IEntryViewProvider;
import com.rpgnextgen.endless.api.IPagedDataProvider;
import com.rpgnextgen.endless.ui.GenericArrayAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// TODO handle not entered username and/or password
// TODO displaying empty view in listview not working
// TODO show spinner on loading source code
// TODO show spinner when loading directory listing

public class IfsActivity extends AppCompatActivity {

    private final String CACHE_CONFIG_ID = "cache.config.id";
    private final String CACHE_PATH = "cache.path";

    private Map<String, ?> configMap;
    private String configId;
    private String configName;
    private String selectedPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ibmi_ifs);

        configureToolbar();

        if (savedInstanceState == null) {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
            selectedPath = savedInstanceState.getString(CACHE_PATH);
        }

        configMap = ConnectionService.get(this, configId);
        configName = configMap.get(ConnectionService.PREF_NAME).toString();

        if (selectedPath == null) {
            selectedPath = configMap.get(IfsConnectionProvider.PREF_PATH).toString();
        }

        IPagedDataProvider<FileHandle> dataProvider = new IfsDataProvider(
                configMap.get(ConnectionService.PREF_SERVER).toString(),
                configMap.get(ConnectionService.PREF_USER).toString(),
                configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                selectedPath
        );
        IEntryViewProvider<FileHandle> viewProvider = new IfsViewProvider(this);
        GenericArrayAdapter<FileHandle> adapter = new GenericArrayAdapter<FileHandle>(this, viewProvider);
        adapter.setDataProvider(dataProvider);
        adapter.new QueryTask().execute();

        final ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FileHandle file = (FileHandle) view.getTag();
                if (file.isDirectory()) {
                    changeDirectory(selectedPath + "/" + file.getName());
                }
                else {
                    new ShowSourceCode(file).execute();
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                listView.setItemChecked(i, !((CheckedTextView) view).isChecked());
                return true;
            }
        });

        updateToolbarTitle();
    }

    private void changeDirectory(String path) {
        selectedPath = path;

        if (selectedPath.startsWith("//")) {
            selectedPath = selectedPath.substring(1);
        }

        ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        listView.clearChoices();
        GenericArrayAdapter<FileHandle> adapter = (GenericArrayAdapter<FileHandle>) listView.getAdapter();
        adapter.clear();
        adapter.getDataProvider().reset();
        ((IfsDataProvider) adapter.getDataProvider()).setPath(selectedPath);
        adapter.new QueryTask().execute();

        updateToolbarTitle();
    }

    private void updateToolbarTitle() {
        String title = configName + ": " + selectedPath;
        getSupportActionBar().setTitle(title);
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ifs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
        outState.putString(CACHE_PATH, selectedPath);

        // TODO save loaded files and map with previous files
    }
    
    public void reload(MenuItem item) {
        ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        GenericArrayAdapter<FileHandle> adapter = (GenericArrayAdapter<FileHandle>) listView.getAdapter();
        adapter.clear();
        adapter.getDataProvider().reset();
        adapter.new QueryTask().execute();
    }

    public void selectAllFiles(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        GenericArrayAdapter<FileHandle> adapter = (GenericArrayAdapter<FileHandle>) listView.getAdapter();
        int count = adapter.getCount();

        for (int i = 0; i < count; i++) {
            listView.setItemChecked(i, true);
        }
    }

    public void deselectAllFiles(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        listView.clearChoices();
        listView.invalidateViews();
    }

    public void changeToParentDirectory(MenuItem item) {
        if (!selectedPath.equals("/")) {
            String parentPath = new File(selectedPath).getParentFile().getPath();
            changeDirectory(parentPath);
        }
    }

    public void download(MenuItem menuItem) {
        List<FileHandle> files = getSelectedFiles();
        if (files.isEmpty()) {
            return;
        }

        new RecursiveDirectoryTreeWalker(files).execute();
    }

    private List<FileHandle> getSelectedFiles() {
        List<FileHandle> selectedFiles = new ArrayList<>();

        ListViewCompat listView = findViewById(R.id.ibmi_ifs_list);
        SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
        int count = checkedItemPositions.size();
        for (int i = 0; i < count; i++) {
            if (checkedItemPositions.valueAt(i)) {
                int checkedListEntryIndex = checkedItemPositions.keyAt(i);
                Object item = listView.getItemAtPosition(checkedListEntryIndex);
                if (item != null) {
                    selectedFiles.add((FileHandle) item);
                }
            }
        }

        return selectedFiles;
    }

    class RecursiveDirectoryTreeWalker extends AsyncTask<Void, Void, List<FileHandle>> {

        private ProgressDialog dialog;
        private List<FileHandle> selectedFiles;
        private boolean cancelled = false;

        private boolean ignoreHiddenFiles = true;

        public RecursiveDirectoryTreeWalker(List<FileHandle> selectedFiles) {
            this.selectedFiles = selectedFiles;
            this.dialog = new ProgressDialog(IfsActivity.this);

            ignoreHiddenFiles = PreferenceManager.getDefaultSharedPreferences(IfsActivity.this).
                                    getBoolean("pref_ignore_hidden_files", true);
        }

        @Override
        protected void onPreExecute() {
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Selecting Sources");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected List<FileHandle> doInBackground(Void... voids) {
            List<FileHandle> files = new ArrayList<>();

            FileConnection connection = null;
            try {
                connection = FileConnection.getConnection(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString());

                for (FileHandle file : selectedFiles) {
                    if (!dialog.isShowing()) {
                        dialog.cancel();
                        cancelled = true;
                        break;
                    }

                    if (ignoreHiddenFiles) {
                        if (file.getName().startsWith(".")) {
                            continue;
                        }
                    }

                    if (!file.isDirectory() && !file.isSymlink()) {
                        files.add(file);
                    }

                    if (file.isDirectory() && !file.isSymlink()) {
                        listFiles(connection, files, file);
                    }
                }
            }
            catch(Exception e) {
                Log.e(this.getClass().getName(), "Could not get directory listing for files.", e);
            }
            finally {
                if (connection != null) {
                    try { connection.close(); }
                    catch(Exception e) { } // ignore
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            return files;
        }

        private void listFiles(FileConnection connection, List<FileHandle> files, FileHandle file) {
            try {
                List<FileHandle> fileHandles = connection.listFiles(file.getPath());
                for (FileHandle f : fileHandles) {
                    if (cancelled) {
                        break;
                    }

                    if (!dialog.isShowing()) {
                        dialog.cancel();
                        break;
                    }

                    if (ignoreHiddenFiles) {
                        if (f.getName().startsWith(".")) {
                            continue;
                        }
                    }

                    if (!f.isDirectory() && !f.isSymlink()) {
                        files.add(f);
                    }

                    if (f.isDirectory() && !file.isSymlink()) {
                        listFiles(connection, files, f);
                    }
                }
            }
            catch(Exception e) {
                Log.e(this.getClass().getName(), "Could not load directory listing for " + file.getPath(), e);
            }
        }

        @Override
        protected void onPostExecute(List<FileHandle> fileHandles) {
            if (!cancelled) {
                new DownloadTask(fileHandles).execute();
            }
        }
    }

    class DownloadTask extends AsyncTask<Void, Void, Integer> {

        private ProgressDialog dialog;
        private List<FileHandle> files;

        public DownloadTask(List<FileHandle> files) {
            this.files = files;
            this.dialog = new ProgressDialog(IfsActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Downloading Source");
            dialog.setMax(files.size());
            dialog.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer successes = 0;
            FileConnection connection = null;

            File storageDirectory = getExternalFilesDir(null);
            if (storageDirectory == null) {
                return null;
            }

            try {
                connection = FileConnection.getConnection(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString());

                for (FileHandle file : files) {
                    String code = null;

                    dialog.setMessage(file.getPath());
                    dialog.setProgress(dialog.getProgress()+1);
                    if (!dialog.isShowing()) {
                        dialog.cancel();
                        return successes;
                    }

                    try {
                        // load content
                        code = ConnectionService.loadSource(connection, file);

                        // write to local file
                        OfflineService.saveSource(IfsActivity.this, IfsActivity.this.configName, file.getPath(), code);

                        successes++;
                    }
                    catch (Exception e) {
                        Log.e(this.getClass().getName(), "Could not download file " + file.getName(), e);
                    }
                    finally {
                        try {
                            if (file.isOpen()) {
                                connection.closeFile(file);
                            }
                        }
                        catch(Exception e) {  } // ignore
                    }
                }
            }
            catch(Exception e) {
                Log.e(this.getClass().getName(), "Could not download files", e);
            }
            finally {
                if (connection != null) {
                    try { connection.close(); }
                    catch(Exception e) { }
                }
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            return successes;
        }

        @Override
        protected void onPostExecute(Integer successes) {
            if (successes == null) {
                Toast.makeText(IfsActivity.this, "Could not downloaded any source for offline viewing. Check if storage is available.", Toast.LENGTH_LONG).show();
            }
            else if (files.size() == successes) {
                Toast.makeText(IfsActivity.this, "Source code successfully downloaded for offline viewing.", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(IfsActivity.this, String.valueOf(successes) + " of " + files.size() + " downloaded for offline viewing.", Toast.LENGTH_LONG).show();
            }
        }
    }

    class ShowSourceCode extends AsyncTask<Void, Void, String> {

        private FileHandle file;

        public ShowSourceCode(FileHandle file) {
            this.file = file;
        }

        @Override
        protected String doInBackground(Void ... voids) {
            try {
                // load source
                return ConnectionService.loadSource(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                        file
                );
            }
            catch (Exception e) {
                Log.e(this.getClass().getName(), "Could not load source for " + file.getPath(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String code) {
            if (code == null) {
                Toast.makeText(IfsActivity.this, "Could not load source code from file " + file.getName(), Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intent = new Intent(IfsActivity.this, SourceCodeActivity.class);
                intent.putExtra("code", code);
                intent.putExtra("path", file.getPath());
                intent.putExtra("configId" , IfsActivity.this.configId);
                intent.putExtra("server", configMap.get(ConnectionService.PREF_SERVER).toString());
                intent.putExtra("connection", configMap.get(ConnectionService.PREF_NAME).toString());
                intent.putExtra("outline", OutlineProviderSelector.getOutlineProvider(file.getName()));
                startActivity(intent);
            }
        }
    }
}
