package com.rpgnextgen.codenotes.ibmi.ifs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;

import java.util.HashMap;
import java.util.Map;

public class EditIfsConnectionDialogActivity extends AppCompatActivity {

    private String configId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connection_ibmi_ifs);

        if (savedInstanceState != null) {
            configId = savedInstanceState.getString(ConnectionService.PREF_ID);

            EditText field = findViewById(R.id.ibmi_source_name);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_NAME, ""));
            // the name should not be changed (create a new connection if you want another name)
            field.setEnabled(false);

            field = findViewById(R.id.ibmi_source_server);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_SERVER, ""));

            field = findViewById(R.id.ibmi_source_user);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_USER, ""));

            field = findViewById(R.id.ibmi_source_password);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_PASSWORD, ""));

            field = findViewById(R.id.ibmi_source_path);
            field.setText(savedInstanceState.getString(IfsConnectionProvider.PREF_PATH, "/"));
        }
        else {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);

            Map<String, ?> config = ConnectionService.get(this, configId);
            EditText field = findViewById(R.id.ibmi_source_name);
            field.setText(config.get(ConnectionService.PREF_NAME).toString());
            // the name should not be changed (create a new connection if you want another name)
            field.setEnabled(false);

            field = findViewById(R.id.ibmi_source_server);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_SERVER)));

            field = findViewById(R.id.ibmi_source_user);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_USER)));

            field = findViewById(R.id.ibmi_source_password);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_PASSWORD)));

            field = findViewById(R.id.ibmi_source_path);
            field.setText(getDefaultString(config.get(IfsConnectionProvider.PREF_PATH)));
        }
    }

    private String getDefaultString(Object o) {
        if (o == null) {
            return "";
        }

        return o.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ibmi_ifs_edit, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(ConnectionService.PREF_ID, configId);

        EditText field = findViewById(R.id.ibmi_source_name);
        outState.putString(ConnectionService.PREF_NAME, field.getText().toString());

        field = findViewById(R.id.ibmi_source_server);
        outState.putString(ConnectionService.PREF_SERVER, field.getText().toString());

        field = findViewById(R.id.ibmi_source_user);
        outState.putString(ConnectionService.PREF_USER, field.getText().toString());

        field = findViewById(R.id.ibmi_source_password);
        outState.putString(ConnectionService.PREF_PASSWORD, field.getText().toString());

        field = findViewById(R.id.ibmi_source_path);
        outState.putString(IfsConnectionProvider.PREF_PATH, field.getText().toString());
    }

    public void deleteConfiguration(MenuItem item) {
        ConnectionService.delete(this, configId);

        Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Configuration deleted", Snackbar.LENGTH_SHORT).show();

        setResult(RESULT_OK);
        finish();
    }

    public void saveConfiguration(MenuItem item) {
        String name = ((EditText) findViewById(R.id.ibmi_source_name)).getText().toString();
        String server = ((EditText) findViewById(R.id.ibmi_source_server)).getText().toString();
        String user = ((EditText) findViewById(R.id.ibmi_source_user)).getText().toString();
        String pass = ((EditText) findViewById(R.id.ibmi_source_password)).getText().toString();

        String rootPath = ((EditText) findViewById(R.id.ibmi_source_path)).getText().toString();

        if (TextUtils.isEmpty(name)) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a name for the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (TextUtils.isEmpty(server)) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a server for the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (TextUtils.isEmpty(user)) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a username for the configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (rootPath.isEmpty()) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter a path where to start browsing.", Snackbar.LENGTH_LONG).show();
        }
        else if (!rootPath.startsWith("/")) {
            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Please enter an absolute root path (starting with /).", Snackbar.LENGTH_LONG).show();
        }
        else {
            String qualifiedConfigurationName = buildConfigurationName(name);

            // save the config
            Map<String,String> config = new HashMap<>();
            config.put(ConnectionService.PREF_ID, qualifiedConfigurationName);
            config.put(ConnectionService.PREF_NAME, name);
            config.put(ConnectionService.PREF_TYPE, IfsConnectionProvider.ID);
            config.put(ConnectionService.PREF_SERVER, server);
            config.put(ConnectionService.PREF_USER, user);
            config.put(ConnectionService.PREF_PASSWORD, pass);
            config.put(IfsConnectionProvider.PREF_PATH, rootPath);
            ConnectionService.save(this, config);

            Snackbar.make(findViewById(R.id.ibmi_section_inner_layout), "Configuration created.", Snackbar.LENGTH_SHORT).show();

            setResult(RESULT_OK);
            finish();
        }
    }

    @NonNull
    private String buildConfigurationName(String name) {
        return this.getClass().getPackage().getName() + "." + name;
    }

}
