package com.rpgnextgen.codenotes.ibmi.qsys;

import android.util.Log;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.endless.api.IPagedDataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MemberDataProvider implements IPagedDataProvider<MemberInfo> {

    private List<MemberInfo> cache = new ArrayList<>();
    private String server;
    private String user;
    private String pass;
    private String library;
    private String file;

    public MemberDataProvider(String server, String user, String pass, String library, String file) {
        this.server = server;
        this.user = user;
        this.pass = pass;
        this.library = library;
        this.file = file;
    }

    @Override
    public List<MemberInfo> getNextBlock() throws IOException {
        if (cache.isEmpty()) {
            load();
        }

        return cache;
    }

    @Override
    public void reset() {
    }

    @Override
    public void reload() throws IOException {
        cache.clear();
        load();
    }

    @Override
    public void setBlockSize(int size) {
        // ignore
    }

    @Override
    public boolean hasMore() {
        return false;
    }

    @Override
    public String getSearchTerm() {
        return null;
    }

    @Override
    public List<MemberInfo> getCached() {
        return cache;
    }

    public void setCached(List<MemberInfo> cache) {
        this.cache.clear();
        this.cache.addAll(cache);
    }

    private void load() {
        try {
            cache.addAll(ConnectionService.loadMembers(server, user, pass, library, file));
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not load members for " + library + "/" + file, e);
        }
    }
}