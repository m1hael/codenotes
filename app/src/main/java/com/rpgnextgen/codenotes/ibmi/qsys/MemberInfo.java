package com.rpgnextgen.codenotes.ibmi.qsys;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Checkable;

public class MemberInfo implements Parcelable, Checkable {

    private boolean checked = false;
    private String name;
    private String file;
    private String library;
    private String type;
    private String description;
    private String contextType;
    private String contextValue;

    public MemberInfo(String name, String file, String library, String type, String description) {
        this.name = name;
        this.file = file;
        this.library = library;
        this.type = type;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public String getLibrary() {
        return library;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getPath() {
        return "/QSYS.LIB/" +
                library.toUpperCase() + ".LIB/" +
                file.toUpperCase() + ".FILE/" +
                name.toUpperCase() + "." + type.toUpperCase();
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public String getContextValue() {
        return contextValue;
    }

    public void setContextValue(String contextValue) {
        this.contextValue = contextValue;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(file);
        parcel.writeString(library);
        parcel.writeString(type);
        parcel.writeString(description);
    }

    public static final Parcelable.Creator<MemberInfo> CREATOR = new Parcelable.Creator<MemberInfo>() {

        @Override
        public MemberInfo createFromParcel(Parcel parcel) {
            MemberInfo memberInfo = new MemberInfo(
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString(),
                    parcel.readString()
            );

            return memberInfo;
        }

        @Override
        public MemberInfo[] newArray(int i) {
            return new MemberInfo[0];
        }
    };

    @Override
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public void toggle() {
        checked = !checked;
    }
}
