package com.rpgnextgen.codenotes.ibmi.qsys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.TextView;
import android.widget.Toast;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.OutlineProviderSelector;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.SourceCodeActivity;
import com.rpgnextgen.codenotes.offline.OfflineService;
import com.rpgnextgen.endless.ui.GenericArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

// TODO handle not entered username and/or password
// TODO displaying empty view in listview not working
// TODO show spinner on loading source code
// TODO show spinner when loading object and member list

public class QsysActivity extends AppCompatActivity {

    private enum Level {
        Libraries, Objects, Members
    }

    private final String CACHE_LEVEL = "cache.level";
    private final String CACHE_CONFIG_ID = "cache.config.id";
    private final String CACHE_LIBRARY = "cache.library";
    private final String CACHE_FILE = "cache.file";
    private final String CACHE_OBJECTS = "cache.objects";
    private final String CACHE_MEMBERS = "cache.members";

    private Map<String, ?> configMap;
    private String configId;
    private String configName;
    private String selectedLibrary = "";
    private String selectedSourceFile = "";
    private String selectedMember = "";
    private Level level;
    private ObjectInfo[] cachedObjectInfos;
    private MemberInfo[] cachedMemberInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ibmi_qsys);

        configureToolbar();

        if (savedInstanceState == null) {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);
            level = Level.Libraries;
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
            level = Level.valueOf(savedInstanceState.getString(CACHE_LEVEL));
            selectedLibrary = savedInstanceState.getString(CACHE_LIBRARY);
            selectedSourceFile = savedInstanceState.getString(CACHE_FILE);
            if (savedInstanceState.containsKey(CACHE_OBJECTS)) {
                cachedObjectInfos = (ObjectInfo[]) savedInstanceState.getParcelableArray(CACHE_OBJECTS);
            }
            if (savedInstanceState.containsKey(CACHE_MEMBERS)) {
                cachedMemberInfos = (MemberInfo[]) savedInstanceState.getParcelableArray(CACHE_MEMBERS);
            }
        }

        configMap = ConnectionService.get(this, configId);
        configName = configMap.get(ConnectionService.PREF_NAME).toString();

        switchAdapter();

        final ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (level) {
                    case Libraries:
                        level = Level.Objects;
                        cachedMemberInfos = null;
                        cachedObjectInfos = null;
                        switchAdapter(((TextView) view).getText().toString());
                        break;

                    case Objects:
                        level = Level.Members;
                        cachedMemberInfos = null;
                        switchAdapter(((ObjectInfo) view.getTag()).getName());
                        break;

                    case Members:
                        selectedMember = ((MemberInfo) view.getTag()).getName();
                        cachedMemberInfos = ((GenericArrayAdapter<MemberInfo>) listView.getAdapter()).getDataProvider().getCached().toArray(new MemberInfo[0]);
                        new ShowSourceCode((MemberInfo) view.getTag()).execute();
                        break;
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch(level) {
                    case Objects:
                        ObjectInfo objectInfo = (ObjectInfo) view.getTag();
                        objectInfo.toggle();
                        listView.invalidateViews();
                        return true;
                    case Members:
                        MemberInfo memberInfo = (MemberInfo) view.getTag();
                        memberInfo.toggle();
                        listView.invalidateViews();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void switchAdapter() {
        switch (level) {
            case Libraries:
                switchAdapter(null);
                break;
            case Objects:
                switchAdapter(selectedLibrary);
                break;
            case Members:
                switchAdapter(selectedSourceFile);
                break;
        }
    }

    private void switchAdapter(String selection) {
        ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);

        switch (level) {
            case Libraries:
                ArrayAdapter<String> libraryAdapter = new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        new ArrayList<String>(Arrays.asList(configMap.get(QsysConnectionProvider.PREF_LIBRARIES).toString().split(","))));
                listView.setAdapter(libraryAdapter);
                break;

            case Objects:
                selectedLibrary = selection;

                ObjectViewProvider objectViewProvider = new ObjectViewProvider(this);
                ObjectDataProvider objectDataProvider = new ObjectDataProvider(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                        selectedLibrary
                );

                GenericArrayAdapter<ObjectInfo> objectAdapter = new GenericArrayAdapter<ObjectInfo>(this, objectViewProvider);
                objectAdapter.setDataProvider(objectDataProvider);
                listView.setAdapter(objectAdapter);

                if (cachedObjectInfos != null) {
                    objectDataProvider.setCached(Arrays.asList(cachedObjectInfos));
                }
                objectAdapter.new QueryTask().execute();
                break;

            case Members:
                // backup object infos for later reuse (but check for adapter because it may be null after orientation change)
                if (listView.getAdapter() != null) {
                    cachedObjectInfos = ((GenericArrayAdapter<ObjectInfo>) listView.getAdapter()).getDataProvider().getCached().toArray(new ObjectInfo[0]);
                }

                selectedSourceFile = selection;

                MemberViewProvider memberViewProvider = new MemberViewProvider(this);
                MemberDataProvider memberDataProvider = new MemberDataProvider(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                        selectedLibrary,
                        selectedSourceFile
                );
                GenericArrayAdapter<MemberInfo> memberAdapter = new GenericArrayAdapter<MemberInfo>(this, memberViewProvider);
                memberAdapter.setDataProvider(memberDataProvider);
                listView.setAdapter(memberAdapter);

                if (cachedMemberInfos != null) {
                    memberDataProvider.setCached(Arrays.asList(cachedMemberInfos));
                }
                memberAdapter.new QueryTask().execute();
                break;
        }

        updateToolbarTitle();
    }

    private void updateToolbarTitle() {
        String title = configName;

        switch (level) {
            case Libraries:
                break;
            case Objects:
                title += " : " + selectedLibrary;
                break;
            case Members:
                title += " : " + selectedLibrary + "/" + selectedSourceFile;
                break;
        }

        getSupportActionBar().setTitle(title);
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qsys, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (level == Level.Libraries) { finish(); }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
        outState.putString(CACHE_LEVEL, level.name());
        outState.putString(CACHE_LIBRARY, selectedLibrary);
        outState.putString(CACHE_FILE, selectedSourceFile);

        ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);

        switch (level) {
            case Objects:
                outState.putParcelableArray(CACHE_OBJECTS,
                        ((GenericArrayAdapter<ObjectInfo>) listView.getAdapter()).getDataProvider().getCached().toArray(new ObjectInfo[0]));
                break;
            case Members:
                outState.putParcelableArray(CACHE_MEMBERS,
                        ((GenericArrayAdapter<MemberInfo>) listView.getAdapter()).getDataProvider().getCached().toArray(new MemberInfo[0]));
                outState.putParcelableArray(CACHE_OBJECTS, cachedObjectInfos);
                break;
        }
    }
    
    public void reload(MenuItem item) {
        try {
            ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);

            switch (level) {
                case Objects:
                    cachedObjectInfos = null;
                    ((GenericArrayAdapter<ObjectInfo>) listView.getAdapter()).clear();
                    ((GenericArrayAdapter<ObjectInfo>) listView.getAdapter()).getDataProvider().reload();
                    ((GenericArrayAdapter<ObjectInfo>) listView.getAdapter()).new QueryTask().execute();
                    break;

                case Members:
                    cachedMemberInfos = null;
                    ((GenericArrayAdapter<MemberInfo>) listView.getAdapter()).clear();
                    ((GenericArrayAdapter<MemberInfo>) listView.getAdapter()).getDataProvider().reload();
                    ((GenericArrayAdapter<MemberInfo>) listView.getAdapter()).new QueryTask().execute();
                    break;
            }
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not reload data.", e);
        }
    }

    public void selectAll(MenuItem menuItem) {
        if (level == Level.Objects || level == Level.Members) {
            final ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);
            List cachedItems = ((GenericArrayAdapter) listView.getAdapter()).getDataProvider().getCached();

            for (Object o : cachedItems) {
                Checkable checkable = (Checkable) o;
                if (!checkable.isChecked()) {
                    checkable.setChecked(true);
                }
            }

            listView.invalidateViews();
        }
    }

    public void deselectAll(MenuItem menuItem) {
        if (level == Level.Objects || level == Level.Members) {
            ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);
            List cachedItems = ((GenericArrayAdapter) listView.getAdapter()).getDataProvider().getCached();

            if (level == Level.Objects || level == Level.Members) {
                for (Object o : cachedItems) {
                    if (o instanceof Checkable) {
                        Checkable checkable = (Checkable) o;
                        checkable.setChecked(false);
                    }
                }
                listView.invalidateViews();
            }
        }
    }

    public void changeToParent(MenuItem item) {
        if (level == Level.Objects) {
            level = Level.Libraries;
            switchAdapter(null);
        }
        else if (level == Level.Members) {
            level = Level.Objects;
            switchAdapter(selectedLibrary);
        }
    }

    public void download(MenuItem menuItem) {
        List<Object> selectedItems = getSelectedItems();
        if (selectedItems.isEmpty()) {
            return;
        }

        new DownloadTask(selectedItems).execute();
    }

    public List<Object> getSelectedItems() {
        List<Object> selectedItems = new ArrayList<>();

        ListViewCompat listView = findViewById(R.id.ibmi_qsys_list);
        List cachedItems = ((GenericArrayAdapter) listView.getAdapter()).getDataProvider().getCached();
        for (Object o : cachedItems) {
            if (o instanceof Checkable) {
                if (((Checkable) o).isChecked()) {
                    selectedItems.add(o);
                }
            }
        }

        return selectedItems;
    }

    class DownloadTask extends AsyncTask<Void, Progress, Integer> {

        private ProgressDialog dialog;
        private List<Object> selectedItems;
        private Integer numberResolvedMembers;
        private boolean cancelled = false;

        public DownloadTask(List<Object> selectedItems) {
            this.selectedItems = selectedItems;
            this.dialog = new ProgressDialog(QsysActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Resolving Members");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(Progress... progresses) {
            Progress progress = progresses[0];
            if (progress.completed == 0) {
                dialog.setMax(progress.total);
                dialog.setIndeterminate(false);
            }
            dialog.setTitle(progress.title);
            dialog.setProgress(progress.completed);

            Log.i(this.getClass().getName(), "progress " + progress.completed);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer successes = 0;

            List<MemberInfo> members = resolveMembers(selectedItems);
            numberResolvedMembers = members.size();

            Progress progress = new Progress("Downloading Source", 0, members.size());
            publishProgress(progress);
            for (MemberInfo member : members) {
                if (!dialog.isShowing()) {
                    dialog.cancel();
                    break;
                }

                try {
                    String code = ConnectionService.loadSource(
                            configMap.get(ConnectionService.PREF_SERVER).toString(),
                            configMap.get(ConnectionService.PREF_USER).toString(),
                            configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                            member);

                    // write to local file
                    OfflineService.saveSource(QsysActivity.this, QsysActivity.this.configName, member.getPath(), code);

                    successes++;
                }
                catch(Exception e) {
                    Log.e(this.getClass().getName(), "Could not download source for member " + member.getName(), e);
                }
                progress.completed++;
                publishProgress(progress);
            }

            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            return successes;
        }

        private List<MemberInfo> resolveMembers(List<Object> selectedItems) {
            List<MemberInfo> members = new ArrayList<>();

            for (Object o : selectedItems) {
                if (o instanceof MemberInfo) {
                    members.add((MemberInfo) o);
                }
                else if (o instanceof ObjectInfo) {
                    ObjectInfo objectInfo = (ObjectInfo) o;

                    try {
                        members.addAll(ConnectionService.loadMembers(
                                configMap.get(ConnectionService.PREF_SERVER).toString(),
                                configMap.get(ConnectionService.PREF_USER).toString(),
                                configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                                objectInfo.getLibrary(), objectInfo.getName()));
                    }
                    catch (Exception e) {
                        Log.e(this.getClass().getName(), "Could not load member for " + objectInfo.getName(), e);
                    }
                }
            }

            return members;
        }

        @Override
        protected void onPostExecute(Integer successes) {
            if (successes == null) {
                Toast.makeText(QsysActivity.this, "Could not downloaded any source for offline viewing. Check if storage is available.", Toast.LENGTH_LONG).show();
            }
            else if (numberResolvedMembers == successes) {
                Toast.makeText(QsysActivity.this, "Source code successfully downloaded for offline viewing.", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(QsysActivity.this, String.valueOf(successes) + " of " + numberResolvedMembers + " downloaded for offline viewing.", Toast.LENGTH_LONG).show();
            }
        }
    }

    class Progress {
        public String title;
        public int completed;
        public int total;

        public Progress(String title, int completed, int total) {
            this.title = title;
            this.completed = completed;
            this.total = total;
        }
    }

    class ShowSourceCode extends AsyncTask<Void, Void, String> {

        private MemberInfo memberInfo;

        public ShowSourceCode(MemberInfo memberInfo) {
            this.memberInfo = memberInfo;
        }

        @Override
        protected String doInBackground(Void ... voids) {
            try {
                // load source
                return ConnectionService.loadSource(
                        configMap.get(ConnectionService.PREF_SERVER).toString(),
                        configMap.get(ConnectionService.PREF_USER).toString(),
                        configMap.get(ConnectionService.PREF_PASSWORD).toString(),
                        memberInfo
                );
            }
            catch (Exception e) {
                Log.e(this.getClass().getName(), "Could not load source for " + memberInfo, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String code) {
            Intent intent = new Intent(QsysActivity.this, SourceCodeActivity.class);
            intent.putExtra("configId", QsysActivity.this.configId);
            intent.putExtra("code", code);
            intent.putExtra("path", memberInfo.getLibrary() + "/" + memberInfo.getFile() + "," + memberInfo.getName());
            intent.putExtra("server", configMap.get(ConnectionService.PREF_SERVER).toString());
            intent.putExtra("connection", configMap.get(ConnectionService.PREF_NAME).toString());
            intent.putExtra("outline", OutlineProviderSelector.getOutlineProvider(memberInfo.getName() + "." + memberInfo.getType()));
            startActivity(intent);
        }

    }
}
