package com.rpgnextgen.codenotes.ibmi;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rpgnextgen.codenotes.OutlineProvider;

import java.util.List;

import miworkplace.ui.editor.cl.ast.Ast;
import miworkplace.ui.editor.cl.ast.File;
import miworkplace.ui.editor.cl.ast.Parameter;
import miworkplace.ui.editor.cl.ast.Parser;
import miworkplace.ui.editor.cl.ast.Subroutine;
import miworkplace.ui.editor.cl.ast.Variable;

// - Outline has linearlayout vertical as main layout
// - every section has a header which is a toggle but which can hide/show content of the section
// - icons for toggle button are + and - (boxed)
// - content of section is indented
// - section content has linearlayout vertical as main layout
// - use button with image and text for "section entries"
// - only show entries if they have content
// - section entry/buttons tag data is line and offset length of source to highlight/show
// - use spans to highlight source

public class ClOutlineProvider implements OutlineProvider {

    @Override
    public View build(Context context, String connection, String code) {
        LinearLayout mainLayout = new LinearLayout(context);
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        Parser parser = new Parser();
        Ast ast = null;

        try {
            ast = parser.parse(code);
        }
        catch(Exception e) {
            TextView errorView = new TextView(context);
            errorView.setText("Error parsing source code: " + e.getMessage());
            mainLayout.addView(errorView);
            return mainLayout;
        }

        try {
            List<Parameter> parameters = ast.listParameters();
            if (!parameters.isEmpty()) {

            }

            List<File> files = ast.listFiles();
            if (!files.isEmpty()) {

            }

            List<Variable> variables = ast.listVariables();
            if (!variables.isEmpty()) {

            }

            List<Subroutine> subroutines = ast.listSubroutines();
            if (!subroutines.isEmpty()) {

            }

            if (mainLayout.getChildCount() == 0) {
                // TODO show message that outline is empty (which is better than seeing nothing at all)
            }

            return mainLayout; // top level view
        }
        catch(Exception e) {
            TextView errorView = new TextView(context);
            errorView.setText("Error filling outline: " + e.getMessage());
            mainLayout.addView(errorView);
            return mainLayout;
        }
    }
}
