package com.rpgnextgen.codenotes.jtopenlite;

import java.io.UnsupportedEncodingException;

import com.ibm.jtopenlite.Conv;
import com.ibm.jtopenlite.command.Parameter;
import com.ibm.jtopenlite.command.Program;

public class ListMembersProgram implements Program {

	private byte[] tempBuffer = new byte[20];

	private final String format = "MBRL0200";
	private String userspaceLibrary;
	private String userspaceName;
	private String library;
	private String file;
	private String name;
	private int ccsid;

	ListMembersProgram(String userspaceLibrary, String userspaceName, String library, String file, String name, int ccsid) {
		this.userspaceLibrary = userspaceLibrary;
		this.userspaceName = userspaceName;
		this.library = library;
		this.file = file;
		this.name = name;
		this.ccsid = ccsid;
	}
	
	public void newCall() {
	}

	public int getNumberOfParameters() {
		return 5;
	}

	public int getParameterInputLength(int parmIndex) {
		switch (parmIndex) {
		case 0:
			return 20;
		case 1:
			return 8;
		case 2:
			return 20;
		case 3:
			return 10;
		case 4:
			return 1;
		default:
			return 0;
		}
	}

	public int getParameterOutputLength(int parmIndex) {
		return 0; // all input parameter => not used
	}

	public int getParameterType(int parmIndex) {
		return Parameter.TYPE_INPUT;
	}

	public byte[] getParameterInputData(int parmIndex) {
		try {
			switch (parmIndex) {
			case 0:
				Conv.stringToBlankPadEBCDICByteArray(userspaceName, tempBuffer, 0, 10, ccsid);
				Conv.stringToBlankPadEBCDICByteArray(userspaceLibrary, tempBuffer, 10, 10, ccsid);
				return tempBuffer;
			case 1:
				Conv.stringToBlankPadEBCDICByteArray(format, tempBuffer, 0, 8, ccsid);
				return tempBuffer;
			case 2:
				Conv.stringToBlankPadEBCDICByteArray(file, tempBuffer, 0, 10, ccsid);
				Conv.stringToBlankPadEBCDICByteArray(library, tempBuffer, 10, 10, ccsid);
				return tempBuffer;
			case 3:
				Conv.stringToBlankPadEBCDICByteArray(name, tempBuffer, 0, 10, ccsid);
				return tempBuffer;
			case 4:
				Conv.stringToEBCDICByteArray("1", tempBuffer, 0, ccsid);
				return tempBuffer;
			}
		}
		catch (UnsupportedEncodingException uee) {
			throw new RuntimeException(uee);
		}
		return null;
	}

	public byte[] getTempDataBuffer() {
		return tempBuffer;
	}

	public void setParameterOutputData(int parmIndex, byte[] tempData, int maxLength) {
		// all input parameter => not used
	}

	public String getProgramName() {
		return "QUSLMBR";
	}

	public String getProgramLibrary() {
		return "QSYS";
	}

}
