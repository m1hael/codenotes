package com.rpgnextgen.codenotes.jtopenlite;

import com.ibm.jtopenlite.Conv;
import com.ibm.jtopenlite.Message;
import com.ibm.jtopenlite.command.CommandConnection;
import com.ibm.jtopenlite.command.CommandResult;
import com.ibm.jtopenlite.command.program.object.CreateUserSpace;
import com.ibm.jtopenlite.command.program.object.DeleteUserSpace;

public class ListMembers {

	// Offsets for format MBRL0200, "List Data Section", of API QUSLMBR.
	private static final int OFFSET_MEMBER_NAME = 0;
	private static final int OFFSET_SOURCE_TYPE = 10;
	private static final int OFFSET_MEMBER_TEXT_DESCRIPTION = 46;

	private final String userspaceLibrary = "QTEMP";
	private final String userspaceName = "JTLITELMBR";

	public MemberInfo[] list(CommandConnection connection, String library, String file, String name) throws Exception {
		MemberInfo[] members;

		CreateUserSpace createUserSpace = new CreateUserSpace(userspaceName, userspaceLibrary,
				CreateUserSpace.EXTENDED_ATTRIBUTE_NONE, // extendedAttribute
				65535, // initialSize
				CreateUserSpace.INITIAL_VALUE_BEST_PERFORMANCE, // initialValue
				CreateUserSpace.PUBLIC_AUTHORITY_USE, // publicAuthority
				"JTOpenLite: ListMembers", // textDescription
				CreateUserSpace.REPLACE_NO, // replace
				CreateUserSpace.DOMAIN_DEFAULT, // domain
				CreateUserSpace.TRANSFER_SIZE_REQUEST_DEFAULT, // transferSizeRequest
				CreateUserSpace.OPTIMUM_SPACE_ALIGNMENT_YES // optimumSpaceAlignment
		);

		CommandResult result = connection.call(createUserSpace);
		if (result.succeeded()) {

			try {
				ListMembersProgram listMembers = new ListMembersProgram(
						userspaceLibrary, userspaceName, library, file, name, connection.getCcsid());
				result = connection.call(listMembers);
				if (result.succeeded()) {
					members = readMembers(connection, userspaceLibrary, userspaceName);
				}
				else {
					throw new RuntimeException("Could not list members in " +
							library + "/" + file + ". " + buildExceptionMessage(result));
				}
			}
			finally {
				DeleteUserSpace deleteUserSpace = new DeleteUserSpace(userspaceName, userspaceLibrary);
				result = connection.call(deleteUserSpace);
				if (!result.succeeded()) {
					throw new RuntimeException("Could not delete userspace " +
							userspaceLibrary + "/" + userspaceName + ". " + buildExceptionMessage(result));
				}
			}

		}
		else {
			throw new RuntimeException("Could not create userspace " +
					userspaceLibrary + "/" + userspaceName + ". " + buildExceptionMessage(result));
		}

		return members;
	}

	private MemberInfo[] readMembers(CommandConnection connection, String userspaceLibrary, String userspaceName) throws Exception {
		byte[] buffer = new byte[65535];

		RetrieveUserSpace retrieveUserSpace = new RetrieveUserSpace(userspaceName, userspaceLibrary, 1, 0x90);
		CommandResult result = connection.call(retrieveUserSpace);
		if (!result.succeeded()) {
			for (Message message : result.getMessagesList()) {
				System.err.println("MSG: " + message.getText());
			}

			throw new RuntimeException("Could not read userspace bytes for generic userspace header.");
		}

		byte[] header = retrieveUserSpace.getContents();

		// Parse the header, to get the offsets to the various sections.

		// (Generic header) Offset to header section:
		int offsetToHeaderSection = Conv.byteArrayToInt(header, 0x74);

		// (Generic header) Header section size:
		int headerSectionSize = Conv.byteArrayToInt(header, 0x78);

		// (Generic header) Offset to list data section:
		int offsetToListDataSection = Conv.byteArrayToInt(header, 0x7C);

		// (Generic header) List data section size:
		int listDataSectionSize = Conv.byteArrayToInt(header, 0x80);

		// (Generic header) Number of list entries:
		int numberOfListEntries = Conv.byteArrayToInt(header, 0x84);

		// (Generic header) Size of each entry:
		int sizeOfEachEntry = Conv.byteArrayToInt(header, 0x88);

		// (Generic header) CCSID of data in the user space
		int entryCcsid = 0;

		if (header.length >= 144) {
			entryCcsid = Conv.byteArrayToInt(header, 0x8C);
		}

		// (Generic header) Subsetted list indicator:
		//String subsettedListIndicator = conv.byteArrayToString(usBuf, 0x95, 1);

		if (entryCcsid == 0) entryCcsid = connection.getCcsid();
		// From the API spec: "The coded character set ID for data in the list entries.  If 0, then the data is not associated with a specific CCSID and should be treated as hexadecimal data."


		// +1 => QUSRTVUS uses 1 based index values
		retrieveUserSpace = new RetrieveUserSpace(userspaceName, userspaceLibrary, offsetToHeaderSection+1, headerSectionSize);
		result = connection.call(retrieveUserSpace);
		if (!result.succeeded()) {
			throw new RuntimeException("Could not read userspace bytes for list header section.");
		}

		buffer = retrieveUserSpace.getContents();

		// (Header section) File name used:
		final String fileNameUsed = Conv.ebcdicByteArrayToString(buffer, 0, 10, entryCcsid).trim();
		// (Header section) File library name used:
		final String libraryNameUsed = Conv.ebcdicByteArrayToString(buffer, 10, 10, entryCcsid).trim();

		// Read the "list data section" into the local buffer.
		if (listDataSectionSize > buffer.length) {
			buffer = new byte[listDataSectionSize+1]; // allocate a larger buffer
		}


		// +1 => QUSRTVUS uses 1 based index values
		retrieveUserSpace = new RetrieveUserSpace(userspaceName, userspaceLibrary, offsetToListDataSection+1, listDataSectionSize);
		result = connection.call(retrieveUserSpace);
		if (!result.succeeded()) {
			throw new RuntimeException("Could not read userspace bytes for list data section.");
		}

		buffer = retrieveUserSpace.getContents();

		MemberInfo[] memberInfos = new MemberInfo[numberOfListEntries];
		byte[] entryBuffer = new byte[sizeOfEachEntry];

		for (int i = 0; i < numberOfListEntries; i++) {
			System.arraycopy(buffer, i * sizeOfEachEntry, entryBuffer, 0, sizeOfEachEntry);
			MemberInfo memberInfo = readMemberEntry(entryBuffer, libraryNameUsed, fileNameUsed, entryCcsid);
			memberInfos[i] = memberInfo;
		}

		return memberInfos;
	}

	private MemberInfo readMemberEntry(byte[] entryBuffer, String library, String file, int ccsid) throws Exception {
		String name = Conv.ebcdicByteArrayToString(entryBuffer, OFFSET_MEMBER_NAME, 10, ccsid).trim();
		String type = Conv.ebcdicByteArrayToString(entryBuffer, OFFSET_SOURCE_TYPE, 10, ccsid).trim();
		String description = Conv.ebcdicByteArrayToString(entryBuffer, OFFSET_MEMBER_TEXT_DESCRIPTION, 50, ccsid).trim();

		return new MemberInfo(name, file, library, type, description);
	}

	private String buildExceptionMessage(CommandResult result) {
		StringBuilder sb = new StringBuilder();

		sb.append("Return code: " + result.getReturnCode());
		sb.append(".\n");

		for (Message message : result.getMessages()) {
			sb.append(message.getID() + " - " + message.getText());
			sb.append("\n");
		}

		return sb.toString();
	}
}