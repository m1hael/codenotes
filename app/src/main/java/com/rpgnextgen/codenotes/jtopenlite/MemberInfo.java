package com.rpgnextgen.codenotes.jtopenlite;

public class MemberInfo {

	private String name;
	private String file;
	private String library;
	private String type;
	private String description;
	
	MemberInfo(String name, String file, String library, String type, String description) {
		this.name = name;
		this.file = file;
		this.library = library;
		this.type = type;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getFile() {
		return file;
	}

	public String getLibrary() {
		return library;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
