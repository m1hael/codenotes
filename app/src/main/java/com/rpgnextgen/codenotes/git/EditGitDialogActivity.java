package com.rpgnextgen.codenotes.git;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EditGitDialogActivity extends AppCompatActivity {

    private String configId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connection_git);

        if (savedInstanceState != null) {
            configId = savedInstanceState.getString(ConnectionService.PREF_ID);

            EditText field = findViewById(R.id.git_repo_name);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_NAME, ""));

            field = findViewById(R.id.git_url);
            field.setText(savedInstanceState.getString(GitConnectionProvider.PREF_URL, ""));

            field = findViewById(R.id.git_branch);
            field.setText(savedInstanceState.getString(GitConnectionProvider.PREF_BRANCH, ""));

            field = findViewById(R.id.git_user);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_USER, ""));

            field = findViewById(R.id.git_password);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_PASSWORD, ""));
        }
        else {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);

            Map<String, ?> config = ConnectionService.get(this, configId);
            EditText field = findViewById(R.id.git_repo_name);
            field.setText(config.get(ConnectionService.PREF_NAME).toString());

            field = findViewById(R.id.git_url);
            field.setText(getDefaultString(config.get(GitConnectionProvider.PREF_URL)));

            field = findViewById(R.id.git_branch);
            field.setText(getDefaultString(config.get(GitConnectionProvider.PREF_BRANCH)));

            field = findViewById(R.id.git_user);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_USER)));

            field = findViewById(R.id.git_password);
            field.setText(getDefaultString(config.get(ConnectionService.PREF_PASSWORD)));
        }

        EditText field = findViewById(R.id.git_repo_name);
        // the name should not be changed (create a new connection if you want another name)
        field.setEnabled(false);
    }

    private String getDefaultString(Object o) {
        if (o == null) {
            return "";
        }

        return o.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_git_edit, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(ConnectionService.PREF_ID, configId);

        EditText field = findViewById(R.id.git_repo_name);
        outState.putString(ConnectionService.PREF_NAME, field.getText().toString());

        field = findViewById(R.id.git_url);
        outState.putString(GitConnectionProvider.PREF_URL, field.getText().toString());

        field = findViewById(R.id.git_branch);
        outState.putString(GitConnectionProvider.PREF_BRANCH, field.getText().toString());

        field = findViewById(R.id.git_user);
        outState.putString(ConnectionService.PREF_USER, field.getText().toString());

        field = findViewById(R.id.git_password);
        outState.putString(ConnectionService.PREF_PASSWORD, field.getText().toString());
    }

    public void deleteConfiguration(MenuItem item) {
        ConnectionService.delete(this, configId);

        Snackbar.make(findViewById(R.id.git_source_layout), "Configuration deleted", Snackbar.LENGTH_SHORT).show();

        setResult(RESULT_OK);
        finish();
    }

    public void saveConfiguration(MenuItem item) {
        String name = ((EditText) findViewById(R.id.git_repo_name)).getText().toString();
        String url = ((EditText) findViewById(R.id.git_url)).getText().toString();
        String user = ((EditText) findViewById(R.id.git_user)).getText().toString();
        String pass = ((EditText) findViewById(R.id.git_password)).getText().toString();
        String branch = ((EditText) findViewById(R.id.git_branch)).getText().toString();

        if (TextUtils.isEmpty(url)) {
            Snackbar.make(findViewById(R.id.git_source_layout), "Please enter an url for the repository.", Snackbar.LENGTH_LONG).show();
        }
        else {
            String qualifiedConfigurationName = buildConfigurationName(name);

            // save the config
            Map<String,String> config = new HashMap<>();
            config.put(ConnectionService.PREF_ID, qualifiedConfigurationName);
            config.put(ConnectionService.PREF_NAME, name);
            config.put(ConnectionService.PREF_TYPE, GitConnectionProvider.ID);
            config.put(ConnectionService.PREF_USER, user);
            config.put(ConnectionService.PREF_PASSWORD, pass);
            config.put(GitConnectionProvider.PREF_URL, url);
            config.put(GitConnectionProvider.PREF_BRANCH, branch);
            ConnectionService.save(this, config);

            Snackbar.make(findViewById(R.id.git_source_layout), "Configuration created.", Snackbar.LENGTH_SHORT).show();

            Intent intent = new Intent();
            intent.putExtra("configuration", qualifiedConfigurationName);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @NonNull
    private String buildConfigurationName(String name) {
        return this.getClass().getPackage().getName() + "." + name;
    }
}
