package com.rpgnextgen.codenotes.git;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.offline.OfflineService;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.util.Map;

public class GitActivity extends AppCompatActivity {

    private final String CACHE_CONFIG_ID = "cache.config.id";

    private Map<String, ?> configMap;
    private String configId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_git);

        configureToolbar();

        if (savedInstanceState == null) {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
        }

        configMap = ConnectionService.get(this, configId);

        updateToolbarTitle();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateToolbarTitle() {
        String title = configMap.get(ConnectionService.PREF_NAME).toString();
        getSupportActionBar().setTitle(title);
    }

    public void download(View view) {
        new CloneRepoTask(configMap).execute();
    }

    class CloneRepoTask extends AsyncTask<Void, Progress, Object> {

        private ProgressDialog dialog;
        private Map<String, ?> configMap;

        public CloneRepoTask(Map<String, ?> configMap) {
            this.configMap = configMap;
            this.dialog = new ProgressDialog(GitActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Cloning Git Repository");
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(Progress... progresses) {
            Progress progress = progresses[0];
            if (progress.completed == 0) {
                dialog.setMax(progress.total);
            }
            dialog.setTitle(progress.title);
            dialog.setProgress(progress.completed);
        }

        @Override
        protected Object doInBackground(Void... voids) {
            OfflineService.deleteConnectionResources(GitActivity.this, configMap.get(ConnectionService.PREF_NAME).toString());
// TODO test redownloading of repo (in same session + in different session)
            File repositoryFolder = OfflineService.getConnectionFolder(GitActivity.this, configMap.get(ConnectionService.PREF_NAME).toString());
            if (!repositoryFolder.exists()) {
                repositoryFolder.mkdirs();
            }

            try {
                CloneCommand command = Git.cloneRepository().
                        setURI(configMap.get(GitConnectionProvider.PREF_URL).toString()).
                        setDirectory(repositoryFolder);

                String branch = configMap.get(GitConnectionProvider.PREF_BRANCH).toString();
                if (!branch.trim().isEmpty()) {
                    command.setBranch(branch);
                }

                command.setProgressMonitor(new ProgressMonitorAdapter() {
                    private Progress progress;

                    @Override
                    public void beginTask(String title, int totalWork) {
                        progress = new Progress(title, 0, totalWork);
                        publishProgress(progress);
                    }

                    @Override
                    public void update(int completed) {
                        progress.completed++;
                        publishProgress(progress);
                    }
                });

                command.call();
                return null;
            }
            catch (GitAPIException e) {
                return e;
            }
            finally {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o == null) {
                // everything is ok
                Toast.makeText(GitActivity.this, "Git repository successfully cloned.", Toast.LENGTH_SHORT).show();
            }
            else if (o instanceof Exception) {
                Toast.makeText(GitActivity.this, "An error occuring during cloning of the repository: " + ((Exception) o).getMessage(), Toast.LENGTH_LONG).show();
                Log.e(this.getClass().getName(), "Error during cloning of repository " + configMap.get(GitConnectionProvider.PREF_URL).toString(), (Throwable) o);
            }
            else {
                Toast.makeText(GitActivity.this, "An error occured during cloning.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class Progress {
        public String title;
        public int completed;
        public int total;

        public Progress(String title, int completed, int total) {
            this.title = title;
            this.completed = completed;
            this.total = total;
        }
    }
}
