package com.rpgnextgen.codenotes.git;

import com.rpgnextgen.codenotes.R;

public class GitConnectionProvider {

    public static final int LOGO_ID = R.drawable.git;
    public static final String name = "Git Repository";
    public static final String PREF_URL = "config.git.url";
    public static final String PREF_BRANCH = "config.git.branch";
    public static final String ID = GitConnectionProvider.class.getPackage().getName();


}
