package com.rpgnextgen.codenotes.github;

import com.rpgnextgen.codenotes.IssueManagementProvider;

public class GithubProvider extends IssueManagementProvider {

    public static final String ID = GithubProvider.class.getPackage().getName();
    public static final String URL = "https://github.com";
    public static final String CONFIG_PROJECT_OWNER = "config.github.project.owner";
    public static final String CONFIG_PROJECT_NAME = "config.github.project.name";
}
