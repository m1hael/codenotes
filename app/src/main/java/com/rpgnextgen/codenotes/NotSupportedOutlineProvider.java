package com.rpgnextgen.codenotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class NotSupportedOutlineProvider implements OutlineProvider {

    @Override
    public View build(Context context, String connection, String code) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.outline_not_supported, null);
    }
}
