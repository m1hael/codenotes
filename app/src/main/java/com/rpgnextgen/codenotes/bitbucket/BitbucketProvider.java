package com.rpgnextgen.codenotes.bitbucket;

import com.rpgnextgen.codenotes.IssueManagementProvider;

public class BitbucketProvider extends IssueManagementProvider {

    public static final String ID = BitbucketProvider.class.getPackage().getName();
    public static final String URL = "https://bitbucket.org";
    public static final String CONFIG_PROJECT_OWNER = "config.bitbucket.project.owner";
    public static final String CONFIG_PROJECT_NAME = "config.bitbucket.project.name";
}
