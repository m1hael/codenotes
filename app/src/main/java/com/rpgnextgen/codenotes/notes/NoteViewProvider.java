package com.rpgnextgen.codenotes.notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckedTextView;

import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.endless.api.IEntryViewProvider;

public class NoteViewProvider implements IEntryViewProvider<Note> {

    private LayoutInflater inflater;

    public NoteViewProvider(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View get(Note note, View view) {
        if (view == null) {
            view = inflater.inflate(R.layout.note_list_entry, null);
        }

        ((CheckedTextView) view).setText(note.getContent());
        view.setTag(note);

        return view;
    }
}
