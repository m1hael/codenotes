package com.rpgnextgen.codenotes.notes;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM note ORDER BY uid ASC")
    List<Note> list();

    @Query("SELECT * FROM note WHERE connection = :connection AND path = :path ORDER BY uid DESC")
    List<Note> list(String connection, String path);

    @Insert
    void insert(Note note);

    @Delete
    void delete(Note note);

    @Delete
    void delete(List<Note> notes);

    @Update
    void update(Note note);
}
