package com.rpgnextgen.codenotes.notes;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.IssueManagementProvider;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.bitbucket.BitbucketProvider;
import com.rpgnextgen.codenotes.github.GithubProvider;
import com.rpgnextgen.codenotes.gitlab.GitlabProvider;
import com.rpgnextgen.endless.ui.GenericArrayAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

// TODO displaying empty view in listview not working
// TODO delete without reassuring dialog but give option to undo delete (like in gmail)

public class NoteListActivity extends AppCompatActivity {

    private final String CACHE_CONFIG_ID = "cache.config.id";
    private final String CACHE_PATH = "cache.path";
    private final String CACHE_CODE = "cache.code";
    private final String CACHE_CONNECTION_NAME = "cache.connection.name";

    private String configId;
    private String path;
    private String code;
    private String connectionName;
    private AlertDialog issueManagementDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_note_list);

        configureToolbar();

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            configId = intent.getStringExtra("configId");
            path = intent.getStringExtra("path");
            code = intent.getStringExtra("code");
            connectionName = intent.getStringExtra("connection");
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
            path = savedInstanceState.getString(CACHE_PATH);
            code = savedInstanceState.getString(CACHE_CODE);
            connectionName = savedInstanceState.getString(CACHE_CONNECTION_NAME);
        }

        GenericArrayAdapter<Note> adapter = new GenericArrayAdapter<Note>(this, new NoteViewProvider(this));
        adapter.setDataProvider(new NoteAdapterDataProvider(this, connectionName, path));
        adapter.new QueryTask().execute();

        ListViewCompat listView = findViewById(R.id.note_list);
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Note note = (Note) view.getTag();
                final EditText contentView = new EditText(NoteListActivity.this);
                contentView.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
                contentView.setText(note.getContent());

                AlertDialog.Builder builder = new AlertDialog.Builder(NoteListActivity.this);
                builder.setTitle("Edit Note");
                builder.setView(contentView);
                builder.setPositiveButton("Save", (dialog, which) -> {
                    note.setContent(contentView.getText().toString());
                    new UpdateNoteTask().execute(note);
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> {
                    dialog.dismiss();
                });
                builder.show();

                return true;
            }
        });

    }

    private List<Note> loadNotes() {
        return CodeNotesDatabase.get(this).noteDao().list();
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
        outState.putString(CACHE_PATH, path);
        outState.putString(CACHE_CODE, code);
        outState.putString(CACHE_CONNECTION_NAME, connectionName);
    }

    public void addNote(MenuItem menuItem) {
        final EditText contentView = new EditText(NoteListActivity.this);
        contentView.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);

        AlertDialog.Builder builder = new AlertDialog.Builder(NoteListActivity.this);
        builder.setTitle("Create Note");
        builder.setView(contentView);
        builder.setPositiveButton("Create", (dialog, which) -> {
            new SaveNoteTask(contentView.getText().toString()).execute();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        AlertDialog dialog = builder.create();
        contentView.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

    public void selectAllNotes(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.note_list);
        GenericArrayAdapter<Note> adapter = (GenericArrayAdapter<Note>) listView.getAdapter();
        int count = adapter.getCount();

        for (int i = 0; i < count; i++) {
            listView.setItemChecked(i, true);
        }
    }

    public void deselectAllNotes(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.note_list);
        listView.clearChoices();
        listView.invalidateViews();
    }

    public void deleteSelectedNotes(MenuItem menuItem) {
        List<Note> selectedNotes = getSelectedNotes();

        new DeleteNotesTask().execute(selectedNotes);
    }

    private List<Note> getSelectedNotes() {
        List<Note> selectedNotes = new ArrayList<>();

        ListViewCompat listView = findViewById(R.id.note_list);
        SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
        int count = checkedItemPositions.size();
        for (int i = 0; i < count; i++) {
            if (checkedItemPositions.valueAt(i)) {
                int checkedListEntryIndex = checkedItemPositions.keyAt(i);
                Object item = listView.getItemAtPosition(checkedListEntryIndex);
                if (item != null) {
                    selectedNotes.add((Note) item);
                }
            }
        }

        return selectedNotes;
    }

    private void refreshListView() {
        ListViewCompat listView = findViewById(R.id.note_list);
        GenericArrayAdapter<Note> adapter = (GenericArrayAdapter<Note>) listView.getAdapter();
        adapter.getDataProvider().reset();
        adapter.clear();
        listView.clearChoices();
        listView.invalidateViews();
        adapter.new QueryTask().execute();
    }

    public void addTask(MenuItem menuItem) {
        Map<String, ?> configMap = ConnectionService.get(this, configId);

        if (configMap.get(IssueManagementProvider.ISSUE_MANAGEMENT_ID) == null) {
            showIssueManagementProviderSelection();
        }
        else {
            // TODO add issue
        }

    }

    private void showIssueManagementProviderSelection() {
        LayoutInflater inflater = NoteListActivity.this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_issue_management, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(NoteListActivity.this);
        builder.setTitle("Choose Issue Management");
        builder.setView(layout);

        issueManagementDialog = builder.create();
        issueManagementDialog.show();
    }

    public void selectIssueManagement(View view) {
        ToggleButton bitbucketButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_bitbucket);
        ToggleButton githubButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_github);
        ToggleButton gitlabButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_gitlab);

        if (view.getTag().equals(BitbucketProvider.ID)) {
            githubButton.setChecked(false);
            gitlabButton.setChecked(false);
        }
        else if (view.getTag().equals(GithubProvider.ID)) {
            bitbucketButton.setChecked(false);
            gitlabButton.setChecked(false);
        }
        else if (view.getTag().equals(GitlabProvider.ID)) {
            bitbucketButton.setChecked(false);
            githubButton.setChecked(false);
        }
        else {
            bitbucketButton.setChecked(false);
            githubButton.setChecked(false);
            gitlabButton.setChecked(false);
        }
    }

    public void setIssueManagement(View view) {
        ToggleButton bitbucketButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_bitbucket);
        ToggleButton githubButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_github);
        ToggleButton gitlabButton = issueManagementDialog.getWindow().findViewById(R.id.issue_management_gitlab);

        if (bitbucketButton.isChecked()) {
            Map<String, String> configMap = (Map<String, String>) ConnectionService.get(this, configId);

            EditText ownerText = issueManagementDialog.getWindow().findViewById(R.id.issue_management_owner);
            EditText nameText = issueManagementDialog.getWindow().findViewById(R.id.issue_management_name);
            String owner = ownerText.getText().toString();
            String name = nameText.getText().toString();

            if (owner.trim().isEmpty()) {
                Toast.makeText(this, "Please enter the project owner.", Toast.LENGTH_LONG).show();
                return;
            }
            else if (name.trim().isEmpty()) {
                Toast.makeText(this, "Please enter the project/repository name.", Toast.LENGTH_LONG).show();
                return;
            }

            configMap.put(IssueManagementProvider.ISSUE_MANAGEMENT_ID, BitbucketProvider.ID);
            configMap.put(BitbucketProvider.CONFIG_PROJECT_OWNER, owner);
            configMap.put(BitbucketProvider.CONFIG_PROJECT_NAME, name);

            ConnectionService.save(this, configMap);
        }

        issueManagementDialog.dismiss();
        issueManagementDialog = null;
    }

    public void deleteIssueManangementReference(MenuItem menuItem) {
        boolean entryRemoved = false;

        Map<String, ?> configMap = ConnectionService.get(this, configId);
        for (String key : new HashSet<>(configMap.keySet())) {
            if (key.startsWith("config.issue")) {
                configMap.remove(key);
                entryRemoved = true;
            }
        }

        if (entryRemoved) {
            ConnectionService.save(this, configMap);
        }
    }

    class UpdateNoteTask extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... notes) {
            Note note = notes[0];
            CodeNotesDatabase.get(NoteListActivity.this).noteDao().update(note);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(NoteListActivity.this, "Updated note", Toast.LENGTH_SHORT).show();
            refreshListView();
        }
    }

    class DeleteNotesTask extends AsyncTask<List<Note>, Void, Void> {

        @Override
        protected Void doInBackground(List<Note>[] notes) {
            try {
                CodeNotesDatabase.get(NoteListActivity.this).noteDao().delete(notes[0]);
            }
            catch(Exception e) {
                Log.e(this.getClass().getName(), "Could not execute task to delete notes.", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            refreshListView();
        }
    }

    class SaveNoteTask extends AsyncTask<Void, Void, Boolean> {

        private String content;

        public SaveNoteTask(String content) {
            this.content = content;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                CodeNotesDatabase database = CodeNotesDatabase.get(NoteListActivity.this);
                database.noteDao().insert(new Note(connectionName, path, content));
                return true;
            }
            catch(Exception e) {
                Log.e(getLocalClassName(), "Could not save note", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            Toast.makeText(NoteListActivity.this, "Added note", Toast.LENGTH_SHORT).show();
            refreshListView();
        }
    }
}
