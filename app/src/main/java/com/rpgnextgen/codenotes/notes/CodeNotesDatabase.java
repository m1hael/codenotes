package com.rpgnextgen.codenotes.notes;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Note.class}, version = 1)
public abstract class CodeNotesDatabase extends RoomDatabase {

    private static CodeNotesDatabase database;

    public static CodeNotesDatabase get(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, CodeNotesDatabase.class, "codenotes")
                    .build();
        }

        return database;
    }

    public abstract NoteDao noteDao();

}
