package com.rpgnextgen.codenotes.notes;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity( indices = { @Index(name = "note_conn_path", unique = false, value = { "connection" , "path" } ) } )
public class Note implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int uid;
    private String connection;
    private String path;
    private String content;
    private String creator;
    private String created;

    public Note() {

    }

    @Ignore
    public Note(String connection, String path, String content) {
        this.connection = connection;
        this.path = path;
        this.content = content;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return connection + ":" + path + "/" + uid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(connection);
        parcel.writeString(content);
        parcel.writeString(created);
        parcel.writeString(creator);
        parcel.writeString(path);
        parcel.writeInt(uid);
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {

        @Override
        public Note createFromParcel(Parcel parcel) {
            Note note = new Note();
            note.setConnection(parcel.readString());
            note.setContent(parcel.readString());
            note.setCreated(parcel.readString());
            note.setCreator(parcel.readString());
            note.setPath(parcel.readString());
            note.setUid(parcel.readInt());
            return note;
        }

        @Override
        public Note[] newArray(int i) {
            return new Note[0];
        }
    };
}
