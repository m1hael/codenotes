package com.rpgnextgen.codenotes.notes;

import android.content.Context;
import android.util.Log;

import com.rpgnextgen.endless.api.IPagedDataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NoteAdapterDataProvider implements IPagedDataProvider<Note> {

    private List<Note> cache = new ArrayList<>();
    private Context context;
    private String connectionName;
    private String path;

    public NoteAdapterDataProvider(Context context, String connectionName, String path) {
        this.context = context;
        this.connectionName = connectionName;
        this.path = path;
    }

    @Override
    public List<Note> getNextBlock() throws IOException {
        if (cache.isEmpty()) {
            load();
        }

        return cache;
    }

    @Override
    public void reset() {
        cache.clear();
    }

    @Override
    public void reload() throws IOException {
        cache.clear();
        load();
    }

    @Override
    public void setBlockSize(int size) {
        // ignore
    }

    @Override
    public boolean hasMore() {
        return false;
    }

    @Override
    public String getSearchTerm() {
        return null;
    }

    @Override
    public List<Note> getCached() {
        return cache;
    }

    public void setCache(List<Note> cache) {
        this.cache.clear();
        this.cache.addAll(cache);
    }

    private void load() {
        try {
            List<Note> notes = CodeNotesDatabase.get(context).noteDao().list(connectionName, path);
            cache.addAll(notes);
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not load notes from database for " + path, e);
        }
    }
}
