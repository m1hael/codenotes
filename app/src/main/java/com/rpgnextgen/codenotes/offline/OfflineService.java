package com.rpgnextgen.codenotes.offline;

import android.content.Context;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OfflineService {

    public static File getConnectionFolder(Context context, String connection) {
        File storageDirectory = context.getExternalFilesDir(null);
        File connectionFolder = new File(storageDirectory.getAbsolutePath() + "/" + connection);
        return connectionFolder;
    }

    public static void deleteConnectionResources(Context context, String connection) {
        File storageDirectory = context.getExternalFilesDir(null);
        File connectionFolder = new File(storageDirectory.getAbsolutePath() + "/" + connection);
        if (connectionFolder.exists()) {
            try {
                FileUtils.deleteDirectory(connectionFolder);
            } catch (IOException e) {
                Log.e(OfflineService.class.getName(), "Could not delete connection folder " + connectionFolder.getAbsolutePath(), e);
            }
        }
    }

    public static void saveSource(Context context, String connection, String path, String code) throws Exception {
        File storageDirectory = context.getExternalFilesDir(null);
        File externalFile = new File(storageDirectory.getAbsolutePath() + "/" + connection + "/" + path);
        Log.d(OfflineService.class.getName(), "Writing code to " + externalFile.getAbsolutePath());

        // create directory structure
        externalFile.getParentFile().mkdirs();

        // create local file
        if (!externalFile.exists()) {
            boolean created = externalFile.createNewFile();
            if (!created) {
                throw new RuntimeException("Could not create file for local copy of " + path);
            }
        }

        // write content to local file
        FileWriter out = new FileWriter(externalFile);
        out.write(code);
        out.flush();
        out.close();
    }

    public static String loadSource(Context context, String connection, String path) throws Exception {
        File storageDirectory = context.getExternalFilesDir(null);
        File externalFile = new File(storageDirectory.getAbsolutePath() + "/" + connection + "/" + path);

        if (!externalFile.exists()) {
            throw new RuntimeException(path + " does not exist.");
        }

        StringBuilder sb = new StringBuilder();
        char[] buffer = new char[8092];
        FileReader reader = new FileReader(externalFile);
        while (reader.read(buffer) >= 0) {
            sb.append(buffer);
        }
        reader.close();

        return sb.toString();
    }

    public static boolean isDirectory(Context context, String connection, String path) {
        File storageDirectory = context.getExternalFilesDir(null);
        File externalFile = new File(storageDirectory.getAbsolutePath() + "/" + connection + "/" + path);

        if (!externalFile.exists()) {
            throw new RuntimeException(path + " does not exist.");
        }

        return externalFile.isDirectory();
    }

    public static List<File> list(Context context, String connection, String path) {
        File storageDirectory = context.getExternalFilesDir(null);
        File externalFile = new File(storageDirectory.getAbsolutePath() + "/" + connection + "/" + path);

        if (!externalFile.exists()) {
            Log.d(OfflineService.class.getName(), externalFile.getAbsolutePath() + " does not exist.");
            return Collections.EMPTY_LIST;
        }

        return Arrays.asList(externalFile.listFiles());
    }
}
