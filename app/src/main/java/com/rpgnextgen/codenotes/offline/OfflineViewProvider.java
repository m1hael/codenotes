package com.rpgnextgen.codenotes.offline;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckedTextView;

import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.endless.api.IEntryViewProvider;

import java.io.File;

public class OfflineViewProvider implements IEntryViewProvider<File> {

    private LayoutInflater inflater;

    public OfflineViewProvider(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View get(File file, View view) {
        if (view == null || !(view instanceof CheckedTextView)) {
            view = inflater.inflate(R.layout.activity_offline_entry, null);
        }

        CheckedTextView fileView = (CheckedTextView) view;
        fileView.setTag(file);
        fileView.setText(file.getName());

        if (file.isDirectory()) {
            fileView.setTypeface(Typeface.DEFAULT_BOLD);
        }
        else {
            // fileView.setPaintFlags(0);
            fileView.setTypeface(Typeface.DEFAULT);
        }

        return fileView;
    }
}
