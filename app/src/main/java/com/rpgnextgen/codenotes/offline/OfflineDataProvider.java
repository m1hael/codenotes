package com.rpgnextgen.codenotes.offline;

import android.content.Context;
import android.util.Log;

import com.rpgnextgen.endless.api.IPagedDataProvider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OfflineDataProvider implements IPagedDataProvider<File> {

    private List<File> cache = new ArrayList<>();
    private String connection;
    private String path;
    private Context context;

    public OfflineDataProvider(Context context, String connection, String path) {
        this.context = context;
        this.connection = connection;
        this.path = path;
    }

    @Override
    public List<File> getNextBlock() throws IOException {
        if (cache.isEmpty()) {
            load();
        }

        return cache;
    }

    @Override
    public void reset() {
        cache.clear();
    }

    @Override
    public void reload() throws IOException {
        cache.clear();
        load();
    }

    @Override
    public void setBlockSize(int size) {
        // ignore
    }

    @Override
    public boolean hasMore() {
        return false;
    }

    @Override
    public String getSearchTerm() {
        return null;
    }

    @Override
    public List<File> getCached() {
        return cache;
    }

    public void setCached(List<File> cache) {
        this.cache.clear();
        this.cache.addAll(cache);
    }

    public void setPath(String path) {
        this.path = path;
    }

    private void load() {
        try {
            cache.addAll(OfflineService.list(context, connection, path));
            Collections.sort(cache);
        }
        catch(Exception e) {
            Log.e(this.getClass().getName(), "Could not load offline files for " + path);
        }
    }
}
