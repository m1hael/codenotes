package com.rpgnextgen.codenotes.offline;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.OutlineProviderSelector;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.SourceCodeActivity;
import com.rpgnextgen.endless.ui.GenericArrayAdapter;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// TODO displaying empty view in listview not working

public class OfflineActivity extends AppCompatActivity {

    private final static String CACHE_CONFIG_ID = "cache.config.id";
    private final static String CACHE_PATH = "cache.path";

    private String configId;
    private Map<String, ?> configMap;
    private String configName;
    private String selectedPath;
    private boolean noContent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);
            selectedPath = "/";
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
            selectedPath = savedInstanceState.getString(CACHE_PATH);
        }

        configMap = ConnectionService.get(this, configId);
        configName = configMap.get(ConnectionService.PREF_NAME).toString();

        if (OfflineService.list(this, configName, selectedPath).isEmpty()) {
            noContent = true;
            setContentView(R.layout.activity_offline_empty);
            configureToolbar();
        }
        else {
            noContent = false;
            setContentView(R.layout.activity_offline);
            configureToolbar();

            OfflineDataProvider dataProvider = new OfflineDataProvider(this, configName, selectedPath);
            OfflineViewProvider viewProvider = new OfflineViewProvider(this);
            GenericArrayAdapter<File> adapter = new GenericArrayAdapter<File>(this, viewProvider);
            adapter.setDataProvider(dataProvider);
            adapter.new QueryTask().execute();

            final ListViewCompat listView = findViewById(R.id.offline_list);
            listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    File file = (File) view.getTag();
                    if (file.isDirectory()) {
                        changeDirectory(selectedPath + "/" + file.getName());
                    } else {
                        new ShowSourceCode(configName, selectedPath + "/" + file.getName()).execute();
                    }
                }
            });
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                    listView.setItemChecked(i, !((CheckedTextView) view).isChecked());
                    return true;
                }
            });

        }
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (noContent) {

        }
        else {
            getMenuInflater().inflate(R.menu.menu_offline, menu);
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
        outState.putString(CACHE_PATH, selectedPath);
    }

    public void changeToParentDirectory(MenuItem item) {
        if (!selectedPath.equals("/")) {
            String parentPath = new File(selectedPath).getParentFile().getPath();
            changeDirectory(parentPath);
        }
    }

    private void changeDirectory(String path) {
        selectedPath = path;

        if (selectedPath.startsWith("//")) {
            selectedPath = selectedPath.substring(1);
        }

        ListViewCompat listView = findViewById(R.id.offline_list);
        if (listView == null) {
            setContentView(R.layout.activity_offline);
            listView = findViewById(R.id.offline_list);
        }

        listView.clearChoices();
        GenericArrayAdapter<File> adapter = (GenericArrayAdapter<File>) listView.getAdapter();
        adapter.clear();
        adapter.getDataProvider().reset();
        ((OfflineDataProvider) adapter.getDataProvider()).setPath(selectedPath);
        adapter.new QueryTask().execute();

        updateToolbarTitle();
    }

    private void updateToolbarTitle() {
        String title = configName + ": " + selectedPath;
        getSupportActionBar().setTitle(title);
    }


    public void selectAll(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.offline_list);
        GenericArrayAdapter<File> adapter = (GenericArrayAdapter<File>) listView.getAdapter();
        int count = adapter.getCount();

        for (int i = 0; i < count; i++) {
            listView.setItemChecked(i, true);
        }
    }

    public void deselectAll(MenuItem menuItem) {
        ListViewCompat listView = findViewById(R.id.offline_list);
        listView.clearChoices();
        listView.invalidateViews();
    }

    private void refreshListView() {
        ListViewCompat listView = findViewById(R.id.offline_list);
        GenericArrayAdapter<File> adapter = (GenericArrayAdapter<File>) listView.getAdapter();
        adapter.getDataProvider().reset();
        adapter.clear();
        listView.clearChoices();
        listView.invalidateViews();
        adapter.new QueryTask().execute();
    }

    public void delete(MenuItem menuItem) {
        List<File> selectedFiles = getSelectedFiles();
        if (!selectedFiles.isEmpty()) {
            new DeleteTask(selectedFiles).execute();
        }
    }

    private List<File> getSelectedFiles() {
        List<File> selectedFiles = new ArrayList<>();

        ListViewCompat listView = findViewById(R.id.offline_list);
        SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
        int count = checkedItemPositions.size();
        for (int i = 0; i < count; i++) {
            if (checkedItemPositions.valueAt(i)) {
                int checkedListEntryIndex = checkedItemPositions.keyAt(i);
                Object item = listView.getItemAtPosition(checkedListEntryIndex);
                if (item != null) {
                    selectedFiles.add((File) item);
                }
            }
        }

        return selectedFiles;
    }

    class DeleteTask extends AsyncTask<Void, Void, Integer> {
        private List<File> files;

        public DeleteTask(List<File> files) {
            this.files = files;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int deletedCount = 0;

            for (File f : files) {
                try {
                    if (f.isDirectory()) {
                        FileUtils.deleteDirectory(f);
                    }
                    else {
                        f.delete();
                    }

                    deletedCount++;
                }
                catch(Exception e) {
                    Log.e(this.getClass().getName(), "Could not delete " + f.getAbsolutePath());
                }
            }

            return deletedCount;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (files.size() == result) {
                Toast.makeText(OfflineActivity.this, "Successfully deleted " + files.size() + " files.", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(OfflineActivity.this, result + " of " + files.size() + " deleted.", Toast.LENGTH_LONG).show();
            }

            refreshListView();
        }
    }

    class ShowSourceCode extends AsyncTask<Void, Void, String> {

        private String configName;
        private String path;

        public ShowSourceCode(String configName, String path) {
            this.configName = configName;
            this.path = path;
        }

        @Override
        protected String doInBackground(Void ... voids) {
            try {
                // load source
                return OfflineService.loadSource(OfflineActivity.this, configName, path);
            }
            catch (Exception e) {
                Log.e(this.getClass().getName(), "Could not load source for " + path, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String code) {
            if (code == null) {
                Toast.makeText(OfflineActivity.this, "Could not load source code from file " + path, Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intent = new Intent(OfflineActivity.this, SourceCodeActivity.class);
                intent.putExtra("configId", OfflineActivity.this.configId);
                intent.putExtra("code", code);
                intent.putExtra("path",path);
                intent.putExtra("connection", configName);
                intent.putExtra("outline", OutlineProviderSelector.getOutlineProvider(path));
                startActivity(intent);
            }
        }
    }

}
