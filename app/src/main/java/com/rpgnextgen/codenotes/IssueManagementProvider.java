package com.rpgnextgen.codenotes;

public class IssueManagementProvider {

    public static final String ISSUE_MANAGEMENT_ID = "config.issue.id";
    public static final String ISSUE_MANAGEMENT_USER = "config.issue.user";
    public static final String ISSUE_MANAGEMENT_PASSWORD = "config.issue.password";

}
