package com.rpgnextgen.codenotes;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.ibm.jtopenlite.command.CommandConnection;
import com.ibm.jtopenlite.components.ListObjects;
import com.ibm.jtopenlite.ddm.DDMConnection;
import com.ibm.jtopenlite.ddm.DDMFile;
import com.ibm.jtopenlite.ddm.DDMReadCallbackAdapter;
import com.ibm.jtopenlite.ddm.DDMRecordFormat;
import com.ibm.jtopenlite.file.FileConnection;
import com.ibm.jtopenlite.file.FileHandle;
import com.rpgnextgen.codenotes.ibmi.qsys.MemberInfo;
import com.rpgnextgen.codenotes.ibmi.qsys.ObjectInfo;
import com.rpgnextgen.codenotes.jtopenlite.ListMembers;
import com.rpgnextgen.codenotes.offline.OfflineService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConnectionService {

    private static final String PREF_CONFIGURATIONS = "configurations";
    public static final String PREF_ID = "config.ID";
    public static final String PREF_TYPE = "config.type";
    public static final String PREF_NAME = "config.name";
    public static final String PREF_SERVER = "config.server";
    public static final String PREF_USER = "config.user";
    public static final String PREF_PASSWORD = "config.pass";

    public static Map<String, ?> get(Context context, String id) {
        SharedPreferences preferences = context.getSharedPreferences(id, Context.MODE_PRIVATE);
        return preferences.getAll();
    }

    public static void save(Context context, Map<String,?> config) {
        SharedPreferences globalPreferences = getGlobalPreferences(context);
        Set<String> configurations =  new HashSet<>(globalPreferences.getStringSet(PREF_CONFIGURATIONS, new HashSet<>()));

        SharedPreferences preferences = context.getSharedPreferences(config.get(PREF_ID).toString(), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit().clear();
        Set<? extends Map.Entry<String, ?>> entries = config.entrySet();
        for (Map.Entry<String, ?> entry : entries) {
            editor.putString(entry.getKey(), entry.getValue().toString());
        }
        editor.commit();

        // add the config to the other configs (to be able to list all available configs)
        if (!configurations.contains(config.get(PREF_ID))) {
            configurations.add(config.get(PREF_ID).toString());
            SharedPreferences.Editor globalEditor = globalPreferences.edit();
            globalEditor.putStringSet(PREF_CONFIGURATIONS, configurations).commit();
        }
    }

    public static boolean exists(Context context, String id) {
        return getGlobalConfigurations(context).contains(id);
    }

    public static void delete(Context context, String id) {
        String configName = get(context, id).get(PREF_NAME).toString();

        SharedPreferences globalPreferences = getGlobalPreferences(context);
        Set<String> configurations = new HashSet<>(globalPreferences.getStringSet(PREF_CONFIGURATIONS, new HashSet<>()));
        configurations.remove(id);
        globalPreferences.edit().putStringSet(PREF_CONFIGURATIONS, configurations).apply();

        OfflineService.deleteConnectionResources(context, configName);
    }

    private static SharedPreferences getGlobalPreferences(Context context) {
        return context.getSharedPreferences("com.rpgnextgen.codenotes", Context.MODE_PRIVATE);
    }

    static List<String> getGlobalConfigurations(Context context) {
        SharedPreferences globalPreferences = getGlobalPreferences(context);
        return new LinkedList<>(globalPreferences.getStringSet(PREF_CONFIGURATIONS, new HashSet<>()));
    }

    public static List<ObjectInfo> loadObjects(String server, String user, String pass, String libraryName) throws Exception {
        List<ObjectInfo> objectInfos = new ArrayList<>();

        CommandConnection connection = CommandConnection.getConnection(server, user, pass);
        try {
            com.ibm.jtopenlite.components.ObjectInfo[] objects = new ListObjects().getObjects(connection, "*ALL", libraryName, "*FILE");
            for (com.ibm.jtopenlite.components.ObjectInfo object : objects) {
                objectInfos.add(map(object));
            }

            return objectInfos;
        }
        finally {
            connection.close();
        }
    }

    private static ObjectInfo map(com.ibm.jtopenlite.components.ObjectInfo object) {
        ObjectInfo objectInfo = new ObjectInfo(
                object.getName().trim(),
                object.getLibrary().trim(),
                object.getType().trim(),
                object.getAttribute().trim(),
                object.getTextDescription().trim()
        );

        return objectInfo;
    }

    public static List<MemberInfo> loadMembers(String server, String user, String pass, String library, String file) throws Exception {
        List<MemberInfo> memberInfos = new ArrayList<>();

        CommandConnection connection = CommandConnection.getConnection(server, user, pass);
        try {
            com.rpgnextgen.codenotes.jtopenlite.MemberInfo[] members = new ListMembers().list(connection, library, file, "*ALL");
            for (com.rpgnextgen.codenotes.jtopenlite.MemberInfo member : members) {
                memberInfos.add(map(member));
            }
            return memberInfos;
        }
        finally {
            connection.close();
        }
    }

    public static List<FileHandle> loadFiles(String server, String user, String pass, String path) throws Exception {
        FileConnection connection = FileConnection.getConnection(server, user, pass);
        try {
            return connection.listFiles(path);
        }
        finally {
            connection.close();
        }
    }

    public static String loadSource(String server, String user, String pass, final FileHandle file) throws Exception {
        FileConnection connection = FileConnection.getConnection(server, user, pass);

        try {
            connection.openFile(file.getPath(), file, FileHandle.SHARE_NONE, FileHandle.OPEN_READ_ONLY, false, file.getDataCCSID());
            try {
                int length = 0;

                if (file.getSize() > Integer.MAX_VALUE) {
                    length = Integer.MAX_VALUE;
                }
                else {
                    length = (int) new BigDecimal(file.getSize()).intValueExact();
                }

                byte[] buffer = new byte[length];
                int bytesRead = connection.readFile(file, buffer, 0, buffer.length);
                return new String(buffer);
            }
            finally {
                connection.closeFile(file);
            }
        }
        finally {
            connection.close();
        }
    }

    public static String loadSource(FileConnection connection, final FileHandle file) throws Exception {
        connection.openFile(file.getPath(), file, FileHandle.SHARE_NONE, FileHandle.OPEN_READ_ONLY, false, file.getDataCCSID());
        try {
            int length = 0;

            if (file.getSize() > Integer.MAX_VALUE) {
                length = Integer.MAX_VALUE;
            }
            else {
                length = (int) new BigDecimal(file.getSize()).intValueExact();
            }

            byte[] buffer = new byte[length];
            int bytesRead = connection.readFile(file, buffer, 0, buffer.length);
            return new String(buffer);
        }
        finally {
            connection.closeFile(file);
        }
    }

    public static String loadSource(String server, String user, String pass, final MemberInfo member) throws Exception {
        final StringBuilder sb = new StringBuilder();

        DDMConnection connection = DDMConnection.getConnection(server, user, pass);
        DDMFile file = null;

        try {
            final DDMRecordFormat recordFormat = connection.getRecordFormat(member.getLibrary(), member.getFile());
            file = connection.open(member.getLibrary(), member.getFile(), member.getName(), recordFormat.getName());

            DDMReadCallbackAdapter callback = new DDMReadCallbackAdapter() {
                @Override
                public void newRecord(int recordNumber, byte[] data, boolean[] nullFieldMap) throws IOException {
                    sb.append(trimr(recordFormat.getField(2).getString(data)));
                    sb.append("\n");
                }
            };

            while (!callback.isDone()) {
                connection.readNext(file, callback);
            }
        }
        finally {
            if (file != null) {
                connection.close(file);
            }
            connection.close();
        }

        return sb.toString();
    }

    private static MemberInfo map(com.rpgnextgen.codenotes.jtopenlite.MemberInfo member) {
        MemberInfo memberInfo = new MemberInfo(
                member.getName(),
                member.getFile(),
                member.getLibrary(),
                member.getType(),
                member.getDescription()
        );

        return memberInfo;
    }

    private static String trimr(String s) {
        if (TextUtils.isEmpty(s.trim())) {
            return "";
        }

        int x = s.length();

        for (int i = s.length()-1; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                x = i+1;
                break;
            }
        }

        if (x >= s.length()) {
            return s;
        }
        else {
            return s.substring(0, x);
        }
    }
}
