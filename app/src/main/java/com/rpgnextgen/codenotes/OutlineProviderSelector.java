package com.rpgnextgen.codenotes;

import com.rpgnextgen.codenotes.ibmi.ClOutlineProvider;

import org.apache.commons.io.FilenameUtils;

public class OutlineProviderSelector {

    public static  OutlineProvider getOutlineProvider(String filename) {
        String suffix = FilenameUtils.getExtension(filename).toUpperCase();
        
        if (suffix.contains("RPG")) {
            return new NotSupportedOutlineProvider();
        }
        else if (suffix.equals("CLE")) {
            // C => not supported
            return new NotSupportedOutlineProvider();
        }
        else if (suffix.contains("CL")) {
            return new ClOutlineProvider();
        }
        else {
            return new NotSupportedOutlineProvider();
        }
    }
}
