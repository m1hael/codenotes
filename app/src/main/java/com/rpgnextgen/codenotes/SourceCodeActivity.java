package com.rpgnextgen.codenotes;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rpgnextgen.codenotes.notes.CodeNotesDatabase;
import com.rpgnextgen.codenotes.notes.Note;
import com.rpgnextgen.codenotes.notes.NoteListActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO s.post(new Runnable() {
//@Override
//public void run() {
//        int y = t.getLayout().getLineTop(40); // e.g. I want to scroll to line 40
//        scrollView.scrollTo(0, y);
//        }
//        });
public class SourceCodeActivity extends AppCompatActivity {

    private final String CACHE_PATH = "cache.path";
    private final String CACHE_CODE = "cache.code";
    private final String CACHE_CONFIG_ID = "cache.config.id";
    private final String CACHE_CONNECTION_NAME = "cache.connection.name";
    private final String CACHE_OUTLINE_PROVIDER = "cache.outline.provider";

    private String configId;
    private String path;
    private String code;
    private String connectionName;
    private OutlineProvider outlineProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_source_code);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            configId = intent.getStringExtra("configId");
            path = intent.getStringExtra("path");
            code = intent.getStringExtra("code");
            connectionName = intent.getStringExtra("connection");
            outlineProvider = (OutlineProvider) intent.getSerializableExtra("outline");
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
            path = savedInstanceState.getString(CACHE_PATH);
            code = savedInstanceState.getString(CACHE_CODE);
            connectionName = savedInstanceState.getString(CACHE_CONNECTION_NAME);
            outlineProvider = (OutlineProvider) savedInstanceState.getSerializable(CACHE_OUTLINE_PROVIDER);
        }

        configureToolbar();

        TextView lineNumberView = findViewById(R.id.source_linenumbers);
        TextView codeView = findViewById(R.id.source_code);
        codeView.setText(code);
        lineNumberView.setText(buildLineNumbers(code));

        ViewGroup outline = findViewById(R.id.source_outline_content);
        View outlineView = outlineProvider.build(this, connectionName, code);
        if (outlineView != null) {
            outline.addView(outlineView);
        }
    }

    private String buildLineNumbers(String code) {
        Pattern pattern = Pattern.compile("\\n");
        Matcher matcher = pattern.matcher(code);

        int count = 0;
        while (matcher.find()) {
            count++;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= count; i++) {
            sb.append(String.valueOf(i));
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_source, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case android.R.id.home:
                 finish();
                 break;
         }

         return super.onOptionsItemSelected(item);
     }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
        outState.putString(CACHE_PATH, path);
        outState.putString(CACHE_CODE, code);
        outState.putString(CACHE_CONNECTION_NAME, connectionName);
        outState.putSerializable(CACHE_OUTLINE_PROVIDER, outlineProvider);
    }

    private void configureToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(connectionName + ": " + path);
    }
    
    public void addNote(MenuItem item) {
        final EditText contentView = new EditText(SourceCodeActivity.this);
        contentView.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);

        AlertDialog.Builder builder = new AlertDialog.Builder(SourceCodeActivity.this);
        builder.setTitle("Create Note");
        builder.setView(contentView);
        builder.setPositiveButton("Create", (dialog, which) -> {
            new SaveNoteTask(contentView.getText().toString()).execute();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        AlertDialog dialog = builder.create();
        contentView.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

    public void listNotes(MenuItem item) {
        Intent intent = new Intent(this, NoteListActivity.class);
        intent.putExtra("path", path);
        intent.putExtra("connection", connectionName);
        intent.putExtra("configId", configId);
        startActivity(intent);
    }

    class SaveNoteTask extends AsyncTask<Void, Void, Boolean> {

        private String content;

        public SaveNoteTask(String content) {
            this.content = content;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                CodeNotesDatabase database = CodeNotesDatabase.get(SourceCodeActivity.this);
                database.noteDao().insert(new Note(connectionName, path, content));
                return true;
            }
            catch(Exception e) {
                Log.e(getLocalClassName(), "Could not save note", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            Toast.makeText(SourceCodeActivity.this, "Added note", Toast.LENGTH_SHORT).show();
        }
    }
}
