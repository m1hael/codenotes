package com.rpgnextgen.codenotes;

import android.content.Context;
import android.view.View;

import java.io.Serializable;

public interface OutlineProvider extends Serializable {

    View build(Context context, String connection, String code);

}
