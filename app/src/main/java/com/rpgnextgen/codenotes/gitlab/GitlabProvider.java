package com.rpgnextgen.codenotes.gitlab;

import com.rpgnextgen.codenotes.IssueManagementProvider;

public class GitlabProvider extends IssueManagementProvider {

    public static final String ID = GitlabProvider.class.getPackage().getName();
    public static final String URL = "https://gitlab.com";
    public static final String CONFIG_PROJECT_OWNER = "config.gitlab.project.owner";
    public static final String CONFIG_PROJECT_NAME = "config.gitlab.project.name";
}
