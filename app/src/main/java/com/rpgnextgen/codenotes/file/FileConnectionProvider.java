package com.rpgnextgen.codenotes.file;

import com.rpgnextgen.codenotes.R;

public class FileConnectionProvider {

    public static final int LOGO_ID = R.drawable.zip_icon;
    public static final String name = "File / Archive";
    public static final String PREF_URL = "config.file.url";
    public static final String ID = FileConnectionProvider.class.getPackage().getName();


}
