package com.rpgnextgen.codenotes.file;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class CreateFileDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connection_file);

        if (savedInstanceState != null) {
            EditText field = findViewById(R.id.file_archive_name);
            field.setText(savedInstanceState.getString(ConnectionService.PREF_NAME, ""));

            field = findViewById(R.id.file_url);
            field.setText(savedInstanceState.getString(FileConnectionProvider.PREF_URL, ""));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_file_create, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        EditText field = findViewById(R.id.file_archive_name);
        outState.putString(ConnectionService.PREF_NAME, field.getText().toString());

        field = findViewById(R.id.file_url);
        outState.putString(FileConnectionProvider.PREF_URL, field.getText().toString());
    }

    public void saveConfiguration(MenuItem item) {
        String name = ((EditText) findViewById(R.id.file_archive_name)).getText().toString();
        String url = ((EditText) findViewById(R.id.file_url)).getText().toString();

        if (TextUtils.isEmpty(name)) {
            Snackbar.make(findViewById(R.id.file_source_layout), "Please enter a name for the file configuration.", Snackbar.LENGTH_LONG).show();
        }
        else if (!ArchiveSupport.isProtocolSupported(url)) {
            Snackbar.make(findViewById(R.id.file_source_layout), "The protocol of the url is not supported.", Snackbar.LENGTH_LONG).show();
        }
        else if (!ArchiveSupport.isArchiveSupported(new File(url).getName())) {
            Snackbar.make(findViewById(R.id.file_source_layout), "The archive type is not supported.", Snackbar.LENGTH_LONG).show();
        }
        else if (TextUtils.isEmpty(url)) {
            Snackbar.make(findViewById(R.id.file_source_layout), "Please enter an url for the file.", Snackbar.LENGTH_LONG).show();
        }
        else {
            String qualifiedConfigurationName = buildConfigurationName(name);

            if (ConnectionService.exists(this, qualifiedConfigurationName)) {
                Snackbar.make(findViewById(R.id.file_source_layout), "The name is already in use. Please use another name for the archive configuration.", Snackbar.LENGTH_LONG).show();
            }
            else {
                // save the config
                Map<String,String> config = new HashMap<>();
                config.put(ConnectionService.PREF_ID, qualifiedConfigurationName);
                config.put(ConnectionService.PREF_NAME, name);
                config.put(ConnectionService.PREF_TYPE, FileConnectionProvider.ID);
                config.put(FileConnectionProvider.PREF_URL, url);
                ConnectionService.save(this, config);

                Snackbar.make(findViewById(R.id.file_source_layout), "Configuration created.", Snackbar.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.putExtra("configuration", qualifiedConfigurationName);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    @NonNull
    private String buildConfigurationName(String name) {
        return this.getClass().getPackage().getName() + "." + name;
    }
}
