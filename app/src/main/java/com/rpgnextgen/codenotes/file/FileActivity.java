package com.rpgnextgen.codenotes.file;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.rpgnextgen.codenotes.ConnectionService;
import com.rpgnextgen.codenotes.R;
import com.rpgnextgen.codenotes.offline.OfflineService;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

public class FileActivity extends AppCompatActivity {

    private final static int IO_BUFFER_SIZE = 32768;

    private final String CACHE_CONFIG_ID = "cache.config.id";

    private Map<String, ?> configMap;
    private String configId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_file);

        configureToolbar();

        if (savedInstanceState == null) {
            configId = getIntent().getStringExtra(ConnectionService.PREF_ID);
        }
        else {
            configId = savedInstanceState.getString(CACHE_CONFIG_ID);
        }

        configMap = ConnectionService.get(this, configId);

        updateToolbarTitle();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(CACHE_CONFIG_ID, configId);
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateToolbarTitle() {
        String title = configMap.get(ConnectionService.PREF_NAME).toString();
        getSupportActionBar().setTitle(title);
    }

    public void download(View view) {
        new DownloadTask(configMap).execute();
    }

    class DownloadTask extends AsyncTask<Void, Progress, Object> {

        private ProgressDialog dialog;
        private Map<String, ?> configMap;

        public DownloadTask(Map<String, ?> configMap) {
            this.configMap = configMap;
            this.dialog = new ProgressDialog(FileActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Download File Archive");
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(Progress... progresses) {
            Progress progress = progresses[0];

            if (progress.indetermined) {
                if (!dialog.isIndeterminate()) {
                    dialog.setIndeterminate(true);
                }
            }
            else {
                if (progress.completed == 0) {
                    dialog.setIndeterminate(false);
                    dialog.setMax(progress.total);
                }
                dialog.setProgress(progress.completed);
            }

            dialog.setTitle(progress.title);
        }

        @Override
        protected Object doInBackground(Void... voids) {
            OfflineService.deleteConnectionResources(FileActivity.this, configMap.get(ConnectionService.PREF_NAME).toString());
// TODO test redownloading of repo (in same session + in different session)
            File archiveFolder = OfflineService.getConnectionFolder(FileActivity.this, configMap.get(ConnectionService.PREF_NAME).toString());
            if (!archiveFolder.exists()) {
                archiveFolder.mkdirs();
            }

            String url = configMap.get(FileConnectionProvider.PREF_URL).toString();
            File tempFile = null;
            try {
                tempFile = download(archiveFolder, url);
                extract(archiveFolder, tempFile);

                return null;
            }
            catch (Exception e) {
                return e;
            }
            finally {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                if (tempFile != null && tempFile.exists()) {
                    tempFile.delete();
                }
            }
        }

        private void extract(File archiveFolder, File file) throws Exception {
            InputStream in = new BufferedInputStream(new FileInputStream(file));
            String type = null;
            try {
                type = CompressorStreamFactory.detect(in);
            }
            catch(CompressorException ce) {
                if (ce.getMessage().toLowerCase().contains("no compressor found")) {
                    // ok => no compressed archive
                }
                else {
                    throw ce;
                }
            }
            finally {
                in.close();
            }

            if (type != null) {

                File archiveFile = new File(FileActivity.this.getCacheDir(), FilenameUtils.getBaseName(file.getName())); // remove the suffix gz/gzip from the filename
                archiveFile.createNewFile();

                OutputStream fout = new BufferedOutputStream(new FileOutputStream(archiveFile));

                CompressorInputStream cin = null;

                final String progressTitle = "Decompressing " + file.getName();
                final int contentLength = new Long(file.length()).intValue();

                try {
                    ProgressInputStream pin = new ProgressInputStream(new FileInputStream(file), contentLength);
                    pin.setListener(new ProgressInputStream.ProgressListener() {
                        @Override
                        public void progress(int completed, int total) {
                            publishProgress(new Progress(progressTitle, completed, total));
                        }
                    });

                    publishProgress(new Progress(progressTitle, 0, contentLength));

                    cin = new CompressorStreamFactory().createCompressorInputStream(new BufferedInputStream(pin));
                    IOUtils.copy(cin, fout);

                    file = archiveFile;
                }
                finally {
                    if (cin != null) {
                        cin.close();
                    }
                    fout.flush();
                    fout.close();
                }
            }

            try {
                type = ArchiveStreamFactory.detect(new BufferedInputStream(new FileInputStream(file)));
            }
            catch(ArchiveException ae) {
                Log.e(this.getClass().getName(), "Error on decompressing archive file.", ae);
                throw new RuntimeException("Error processing archive. Probably a not supported archive type.");
            }


            final String progressTitle = "Extracting content";
            final int contentLength = new Long(file.length()).intValue();

            publishProgress(new Progress(progressTitle, 0, contentLength));

            ProgressInputStream pin = new ProgressInputStream(new BufferedInputStream(new FileInputStream(file), IO_BUFFER_SIZE), contentLength);
            pin.setListener(new ProgressInputStream.ProgressListener() {
                @Override
                public void progress(int completed, int total) {
                    publishProgress(new Progress(progressTitle, completed, contentLength));
                }
            });
            in = pin;

            ArchiveInputStream ain = new ArchiveStreamFactory().createArchiveInputStream(in);
            ArchiveEntry archiveEntry = null;

            if (type.equals(ArchiveStreamFactory.JAR)) {
                while ((archiveEntry = ain.getNextEntry()) != null) {
                    JarArchiveEntry entry = (JarArchiveEntry) archiveEntry;
                    if (!entry.isDirectory() && !entry.isUnixSymlink()) {
                        byte[] buffer = new byte[new Long(entry.getSize()).intValue()];
                        ain.read(buffer);

                        // entry names also contains parent directories
                        File f = new File(archiveFolder.getAbsolutePath() + "/" + entry.getName());
                        f.getParentFile().mkdirs();
                        OutputStream out = new BufferedOutputStream(new FileOutputStream(f), IO_BUFFER_SIZE);
                        out.write(buffer);
                        out.flush();
                        out.close();
                    }
                }
            }
            else if (type.equals(ArchiveStreamFactory.SEVEN_Z)) {
                while ((archiveEntry = ain.getNextEntry()) != null) {
                    SevenZArchiveEntry entry = (SevenZArchiveEntry) archiveEntry;
                    if (!entry.isDirectory()) {
                        byte[] buffer = new byte[new Long(entry.getSize()).intValue()];
                        ain.read(buffer);

                        // entry names also contains parent directories
                        File f = new File(archiveFolder.getAbsolutePath() + "/" + entry.getName());
                        f.getParentFile().mkdirs();
                        OutputStream out = new BufferedOutputStream(new FileOutputStream(f), IO_BUFFER_SIZE);
                        out.write(buffer);
                        out.flush();
                        out.close();
                    }
                }
            }
            else if (type.equals(ArchiveStreamFactory.TAR)) {
                while ((archiveEntry = ain.getNextEntry()) != null) {
                    TarArchiveEntry entry = (TarArchiveEntry) archiveEntry;
                    if (!entry.isDirectory() && !entry.isSymbolicLink()) {
                        byte[] buffer = new byte[new Long(entry.getSize()).intValue()];
                        ain.read(buffer);

                        // entry names also contains parent directories
                        File f = new File(archiveFolder, entry.getName());
                        f.getParentFile().mkdirs();
                        OutputStream out = new BufferedOutputStream(new FileOutputStream(f), IO_BUFFER_SIZE);
                        out.write(buffer);
                        out.flush();
                        out.close();
                    }
                }
            }
            else if (type.equals(ArchiveStreamFactory.ZIP)) {
                while ((archiveEntry = ain.getNextEntry()) != null) {
                    ZipArchiveEntry entry = (ZipArchiveEntry) archiveEntry;
                    if (!entry.isDirectory() && !entry.isUnixSymlink()) {
                        byte[] buffer = new byte[new Long(entry.getSize()).intValue()];
                        ain.read(buffer);

                        // entry names also contains parent directories
                        File f = new File(archiveFolder, entry.getName());
                        f.getParentFile().mkdirs();
                        OutputStream out = new BufferedOutputStream(new FileOutputStream(f), IO_BUFFER_SIZE);
                        out.write(buffer);
                        out.flush();
                        out.close();
                    }
                }
            }
        }

        private File download(File archiveFolder, String sourceUrl) throws Exception {
            URL url = new URL(sourceUrl);

            File tempFile = new File(FileActivity.this.getCacheDir(), FilenameUtils.getName(url.getFile()));
            tempFile.createNewFile();

            if (URLUtil.isHttpUrl(sourceUrl) || URLUtil.isHttpsUrl(sourceUrl)) {
                final String progressTitle = "Downloading " + FilenameUtils.getName(url.getFile());

                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(sourceUrl);

                publishProgress(new Progress(progressTitle, Progress.INDETERMINED));

                // Execute the request
                HttpResponse response = httpclient.execute(httpget);
                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(tempFile), IO_BUFFER_SIZE);
                    entity.writeTo(out);
                    out.flush();
                    out.close();
                }
                else {
                    throw new RuntimeException("Could not download file. HTTP code: " +
                            response.getStatusLine().getStatusCode() + " - " + response.getStatusLine().getReasonPhrase());
                }
            }
            else if (URLUtil.isFileUrl(sourceUrl)) {
                File sourceFile = new File(url.toURI());
                final String progressTitle = "Copying " + sourceFile.getName();
                final int contentLength = new Long(sourceFile.length()).intValue();

                publishProgress(new Progress(progressTitle, 0, contentLength));

                ProgressInputStream pin = new ProgressInputStream(new FileInputStream(sourceFile), contentLength);
                pin.setListener(new ProgressInputStream.ProgressListener() {
                    @Override
                    public void progress(int completed, int total) {
                        publishProgress(new Progress(progressTitle, completed, total));
                    }
                });

                InputStream in = new BufferedInputStream(pin, IO_BUFFER_SIZE);
                OutputStream out = new FileOutputStream(tempFile);
                IOUtils.copy(in, out);
                out.flush();
                out.close();
                in.close();
            }
            else {
                throw new RuntimeException("Protocol " + url.getProtocol() + " is not supported. Supported protocols are file, http, https.");
            }

            return tempFile;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o == null) {
                // everything is ok
                Toast.makeText(FileActivity.this, "File archive successfully downloaded and extracted.", Toast.LENGTH_SHORT).show();
            }
            else if (o instanceof Exception) {
                Toast.makeText(FileActivity.this, "An error occured during downloading of the file archive: " + ((Exception) o).getMessage(), Toast.LENGTH_LONG).show();
                Log.e(this.getClass().getName(), "Error during downloading of file archive " + configMap.get(FileConnectionProvider.PREF_URL).toString(), (Throwable) o);
            }
            else {
                Toast.makeText(FileActivity.this, "An error occured during downloading and extracting.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class Progress {
        public final static boolean INDETERMINED = true;
        public final static boolean NON_INDETERMINED = false;

        public String title;
        public int completed;
        public int total;
        public boolean indetermined = false;

        public Progress(String title, int completed, int total) {
            this.title = title;
            this.completed = completed;
            this.total = total;
        }

        public Progress(String title, boolean indetermined) {
            this.title = title;
            this.indetermined = indetermined;
        }
    }
}
