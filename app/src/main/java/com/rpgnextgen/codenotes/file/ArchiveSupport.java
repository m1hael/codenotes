package com.rpgnextgen.codenotes.file;

import android.webkit.URLUtil;

public class ArchiveSupport {

    public static boolean isProtocolSupported(String url) {
        if (URLUtil.isFileUrl(url)) {
            return true;
        }
        else if (URLUtil.isHttpsUrl(url)) {
            return true;
        }
        else if (URLUtil.isHttpUrl(url)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isArchiveSupported(String filename) {
        filename = filename.toLowerCase();

        if (filename.endsWith("zip")) {
            return true;
        }
        else if (filename.endsWith("7z")) {
            return true;
        }
        else if (filename.endsWith("gzip")) {
            return true;
        }
        else if (filename.endsWith("gz")) {
            return true;
        }
        else if (filename.endsWith("bz")) {
            return true;
        }
        else if (filename.endsWith("bzip")) {
            return true;
        }
        else if (filename.endsWith("bzip2")) {
            return true;
        }
        else if (filename.endsWith("bz2")) {
            return true;
        }
        else if (filename.endsWith("tar")) {
            return true;
        }
        else if (filename.endsWith("jar")) {
            return true;
        }

        return false;
    }
}
