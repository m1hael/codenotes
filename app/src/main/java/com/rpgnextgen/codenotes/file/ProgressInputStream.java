package com.rpgnextgen.codenotes.file;

import android.renderscript.ScriptGroup;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class ProgressInputStream extends FilterInputStream {

    final int total;
    final AtomicInteger currentProgress = new AtomicInteger(0);

    public interface ProgressListener {
        void progress(int completed, int total);
    }

    private ProgressListener listener;

    public ProgressInputStream(InputStream in, int total) {
        super(in);
        this.total = total;
    }

    public ProgressInputStream(InputStream in, int total, ProgressListener listener) {
        super(in);
        this.total = total;
        this.listener = listener;
    }

    @Override
    public int read() throws IOException {
        int b = super.read();
        updateProgress(1);
        return b;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return updateProgress(super.read(b));
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return updateProgress(super.read(b, off, len));
    }

    @Override
    public long skip(long n) throws IOException {
        return (long) updateProgress((int) super.skip(n));
    }

    @Override
    public void mark(int readlimit) {
        super.mark(readlimit);
    }

    @Override
    public void reset() throws IOException {
        super.reset();
    }

    @Override
    public boolean markSupported() {
        return super.markSupported();
    }

    public void setListener(ProgressListener listener) {
        this.listener = listener;
    }

    private int updateProgress(int numBytesRead) {
        if (numBytesRead > 0 && listener != null) {
            try { listener.progress(currentProgress.addAndGet(numBytesRead), total); }
            catch (Exception e) { }
        }

        return numBytesRead;
    }
}
