package com.rpgnextgen.endless.api;

import java.io.IOException;
import java.util.List;

/**
 * Data provider for the adapter filling the view.
 * <p>
 * The data provider provides the adapter with the entries which will be displayed in the view. 
 * The data provider has to keep track of what entries have already been displayed and what entries
 * will be displayed next.
 * <p>
 * Prefetching and caching is all up to the implementation.
 * 
 * @author Mihael Schmidt
 *
 * @param <T> Type of data model
 */
public interface IPagedDataProvider<T> {

    /**
     * Gets the next block of entries.
     * 
     * @return List of entries
     * 
     * @throws IOException Throws an IOException if there is an error in the communication with 
     *                     the data source (f. e. web service).
     */
    List<T> getNextBlock() throws IOException;
    
    /**
     * Resets the data provider which at least resets the internal mechanism for providing the 
     * batches of entries. After calling <code>reset()</code> the first batch of entries should be
     * returned by {@link #getNextBlock()}.   
     */
    void reset();
    
    /**
     * Reloads the number of entries. A reload does not reset the iterator mechanism providing the
     * batch of entries.
     * 
     * @throws IOException Throws an IOException if there is an error in the communication with 
     *                     the data source (f. e. web service).
     */
    void reload() throws IOException;
    
    /**
     * Sets the block size of the entries retrieved from this data provider via the
     * {@link #getNextBlock()} method.
     * 
     * @param size block size
     */
    void setBlockSize(int size);
    
    /**
     * Returns if the data provider has more entries to return with {@link #getNextBlock()}.
     * 
     * @return <code>true</code> if the provider has more entries to return otherwise <code>false</code>
     */
    boolean hasMore();

    /**
     * Returns the search term for the current search if a search term is used else it returns 
     * <code>null</code>
     * 
     * @return Search term or <code>null</code> if no search term has been used
     */
    String getSearchTerm();
    

    /**
     * Returns a list of cached entries which will be presented at the beginning of the list
     * (f. e. on an orientation change of the android device).
     *
     * @return List of cached entries
     */
    List<T> getCached(); 
}
