package com.rpgnextgen.endless.api;

/**
 * Handler for the case if the result set for the list view is empty.
 * 
 * @author Mihael Schmidt
 *
 */
public interface IEmptyResultSetHandler {

	void handleEmptyResult(String searchTerm);
}
