package com.rpgnextgen.endless.api;

import android.view.View;

/**
 * List entry view provider for providing views for list entries.
 * 
 * @author Mihael Schmidt
 *
 * @param <T> Data model class
 */
public interface IEntryViewProvider<T> {

    /**
     * Returns a single view or a composition of views which represent a list entry in the list view.
     * <p>
     * If a view should be reused as a list entry it will be passed as the second parameter.
     * <p>
     * The data model object should be tagged ({@link View#setTag(Object)}) to the view.
     * (though not a requirement of the framework, just for later reuse when the entry is selected).
     *  
     * @param entry entry data model
     * @param view reusable view or <code>null</code> if a new view should be created
     * 
     * @return view for the passed entry
     */
    View get(T entry, View view);
}
