package com.rpgnextgen.endless.api;

import com.rpgnextgen.endless.ui.GenericArrayAdapter;

/**
 * Error handler for data provider.
 * 
 * @author Mihael Schmidt
 *
 */
public interface IDataProviderErrorHandler {

    /**
     * This method will be called by the {@link GenericArrayAdapter} when an error occurs in the 
     * fetching of the next data block.
     *  
     * @param e Exception which occurred during the fetch
     */
    void handleError(Exception e);
}
