package com.rpgnextgen.endless.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.rpgnextgen.endless.api.IDataProviderErrorHandler;

public class DataProviderErrorHandler implements IDataProviderErrorHandler {

    private final Context context; 
    private final String message;

    public DataProviderErrorHandler(Context context, String message) {
        this.context = context;
        this.message = message;
    }
    
    @Override
    public void handleError(Exception e) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        Log.e(this.getClass().getName(), "There was an error getting the next block of data " +
                "from the data provider.", e);
    }

    public Context getContext() {
        return context;
    }
}
