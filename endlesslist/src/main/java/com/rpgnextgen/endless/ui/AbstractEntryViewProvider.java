package com.rpgnextgen.endless.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.rpgnextgen.endless.R;
import com.rpgnextgen.endless.api.IEntryViewProvider;

/**
 * Abstract entry view provider which provides the endless list adapter with new views. This class
 * doesn't fill the views with data it just create instances of the views if needed. The implementing
 * class does have to take care of filling the views with data, see {@link #fillView(View, Object)}.
 * 
 * @author Mihael Schmidt
 *
 * @param <T> data model class
 */
public abstract class AbstractEntryViewProvider<T> implements IEntryViewProvider<T> {

    private final LayoutInflater inflater;
    private Integer layoutId = R.layout.entry;
    
    public AbstractEntryViewProvider(Context context) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    public AbstractEntryViewProvider(Context context, Integer layoutId) {
        this.layoutId = layoutId;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    public View get(T entry, View view) {
        View retView;
        
        if (view == null) {
            retView = (TextView) inflater.inflate(layoutId, null);
        }
        else if (view instanceof TextView) {
            retView = (TextView) view;
        }
        else {
            retView = (TextView) inflater.inflate(layoutId, null);
        }
        
        fillView(retView, entry);

        retView.setTag(entry);
        
        return retView;
    }

    public abstract void fillView(View view, T entry);

}
