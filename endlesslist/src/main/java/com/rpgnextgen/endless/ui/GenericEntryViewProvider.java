package com.rpgnextgen.endless.ui;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.rpgnextgen.endless.model.Entry;

/**
 * Generic entry view provider which uses the {@link Entry} class as the data model and expects a
 * TextView as the target view for the text to be set. The TextView can be nested inside other views.
 * It will be found by the passed text view id. 
 * <p>
 * By default it is assumed that the data model has an attribute with the key <em>name</em>. 
 * Optionally the attribute key for the text can be passed to the constructor, 
 * {@link GenericEntryViewProvider#GenericEntryViewProvider(Context, int, String)}
 * 
 * @author Mihael Schmidt
 *
 */
public class GenericEntryViewProvider extends AbstractEntryViewProvider<Entry> {

    private final String attribute;
    private final int textViewId;
    
    public GenericEntryViewProvider(Context context, int textViewId) {
        super(context);
        this.attribute = "name";
        this.textViewId = textViewId;
    }
    
    public GenericEntryViewProvider(Context context, int textViewId, String attribute) {
        super(context); 
        this.textViewId = textViewId;
        this.attribute  = attribute;
    }
    
    public GenericEntryViewProvider(Context context, int layoutid, int textViewId) {
        super(context, layoutid);
        this.attribute = "name";
        this.textViewId = textViewId;
    }
    
    public GenericEntryViewProvider(Context context, int layoutId, int textViewId, String attribute) {
        super(context, layoutId);
        this.attribute = attribute;
        this.textViewId = textViewId;
    }
    
    @Override
    public void fillView(View view, Entry entry) {
        String value = entry.get(attribute);
        
        TextView v = (TextView) view.findViewById(textViewId);
        if (v != null) {
            v.setText(value);
        }
    }
}
