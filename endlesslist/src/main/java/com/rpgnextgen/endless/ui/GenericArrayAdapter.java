package com.rpgnextgen.endless.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.rpgnextgen.endless.R;
import com.rpgnextgen.endless.api.IDataProviderErrorHandler;
import com.rpgnextgen.endless.api.IEmptyResultSetHandler;
import com.rpgnextgen.endless.api.IEntryViewProvider;
import com.rpgnextgen.endless.api.IPagedDataProvider;

/**
 * An Adapter implementation which dynamically serves views to the {@link ListView}. The Adapter 
 * will serve views in batch. If there are more views available then a <em>more</em> view will be 
 * shown which on touch loads the next batch of views. The size of the batch depends on the 
 * {@link IPagedDataProvider}.
 * <p>
 * This class can handle single or complex views as list entries. The view will be filled with
 * data by the {@link IEntryViewProvider} implementation. 
 * <p>
 * The data for the views is provided by the {@link IPagedDataProvider} implementation. 
 * The data does not has to be loaded all at once. It can be loaded dynamically. Prefetching and
 * caching is up to the data provider. If no data provider is set this class will return 0 elements 
 * on the element count, {@link #getCount()}.
 * <p>
 * If there occurs a problem with getting the data from the data source the error handler is called
 * and can take some action (like informing the user).
 * 
 * @author Mihael Schmidt
 *
 * @param <T> Data model class
 */
public class GenericArrayAdapter<T> extends ArrayAdapter<T> implements OnClickListener {

    private static final int BUTTON_ID = -1;
    private static final int SPINNER_ID = -2;
    
    private boolean loading = false;
    
    private LayoutInflater inflater;
    private View more;
    private View spinner;
    private final IEntryViewProvider<T> viewProvider;
    private IPagedDataProvider<T> dataProvider;
    private IDataProviderErrorHandler errorHandler;
    private IEmptyResultSetHandler emptyHandler;
    
    /**
     * Standard constructor.
     * 
     * @param context Context
     * @param viewProvider View provider for list entries
     */
    public GenericArrayAdapter(Context context, IEntryViewProvider<T> viewProvider) {
        super(context, 0, 0, new ArrayList<T>());
        this.viewProvider = viewProvider;
        
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.spinner = inflater.inflate(R.layout.entry_spinner,null);
        this.more = inflater.inflate(R.layout.entry_more, null);
        this.more.setOnClickListener(this);
    }
    
    /**
     * Extended constructor which support a custom view for the more button/view.
     * 
     * @param context Context
     * @param viewProvider View provider for list entries
     * @param more View for more button
     */
    public GenericArrayAdapter(Context context, IEntryViewProvider<T> viewProvider, View more) {
        super(context, 0, 0, new ArrayList<T>());
        this.viewProvider = viewProvider;
        
        this.more = more;
        this.more.setOnClickListener(this);
        
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.spinner = inflater.inflate(R.layout.entry_spinner,null);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        
        long itemId = getItemId(position);
        
        if (itemId == BUTTON_ID) {
            view = more;
        }
        else if (itemId == SPINNER_ID) {
            view = spinner;
        }
        else {

            T entry = super.getItem(position);
            
            // old view components will be recycled/reused
            // the system will also try to recycle the button 
            // so we need to check also for the button
            if (convertView == null || convertView instanceof Button || convertView instanceof ProgressBar) {
                view = viewProvider.get(entry, null);
            }
            else {
                view = viewProvider.get(entry, view);
            }
            
        }
        
        return view;
    }
    
    @Override
    public int getCount() {
        if (dataProvider == null) {
            return 0;
        }
        else {
            if (dataProvider.hasMore()) {
                return super.getCount() + 1;
            }
            else {
                return super.getCount();
            }
        }
    }
    
    @Override
    public T getItem(int position) {
        if (super.getCount() > position) {
            return super.getItem(position);
        }
        else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * 
     * If a <em>more</em> view or <em>spinner</em> view is also served 
     * then this method will also take the extra view into account.
     */
    @Override
    public long getItemId(int position) {
        if (super.getCount() > position) {
            return super.getItemId(position);
        }
        else if (isLoading()) {
            return SPINNER_ID;
        }
        else {
            return BUTTON_ID;
        }
    }
    
    /**
     * Fetches the next block of data from the data provider and adds it to the list of data objects
     * which are displayed in the list view.
     */
    @Override
    public void onClick(View view) {
        new QueryTask().execute();
    }
    
    /**
     * Sets the data provider for the list view. If the data provider has some cached values they will
     * be added to to the adapter.
     *
     * @param dataProvider New data provider
     */
    public void setDataProvider(IPagedDataProvider<T> dataProvider) {
        this.dataProvider = dataProvider;
        
        List<T> cachedEntries = dataProvider.getCached();
        if (cachedEntries != null) {
        	addAll(cachedEntries);
        }
    } 
    
    /**
     * Getting the error handler for the data provider.
     * 
     * @return Data provider error handler or <code>null</code> if not set
     */
    public IDataProviderErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /**
     * Setting the error handler for the data provider.
     * 
     * @param errorHandler Data provider error handler
     */
    public void setErrorHandler(IDataProviderErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    /**
     * Sets the handler for empty results sets.
     * 
     * @param emptyResultHandler Empty result handler
     */
    public void setEmptyResultHandler(IEmptyResultSetHandler emptyResultHandler) {
    	this.emptyHandler = emptyResultHandler;
    }
    
    /**
     * Returns the empty result handler.
     * 
     * @return Handler or <code>null</code>
     */
    public IEmptyResultSetHandler getEmptyResultHandler() {
    	return emptyHandler;
    }
    
    private boolean isLoading() {
        return loading;
    }

    /**
     * Controls if the spinner is displayed in the UI.
     * 
     * @param loading <code>true</em> for showing the loading spinner view
     */
    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    private GenericArrayAdapter<T> current() {
        return this;
    }
 
    /**
     * Returns a list of the displayed data.
     * 
     * @return List of data model objects
     */
    public List<T> getData() {
        List<T> data = new ArrayList<T>();
        
        int size = super.getCount();
        
        for (int i = 0; i < size; i++) {
            data.add(getItem(i));
        }
        
        return data;
    }

    public IPagedDataProvider<T> getDataProvider() {
        return dataProvider;
    }

    /**
     * An AsyncTask which will try to fetch the next block of data and add it to the list adapter. It
     * is no requirement to <em>only</em> use <em>this</em> class for loading the data. Any custom class
     * can be used.
     * <p>
     * Note: Don't forget to set {@link GenericArrayAdapter#setLoading(boolean)} to true when loading the
     *       data, so that the user gets a spinner display while waiting.
     *       
     * @author Mihael Schmidt
     *
     */
    public class QueryTask extends AsyncTask<Void, Void, List<T>> {

        private Exception e = null;
        
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            
            setLoading(true);
        }
        
        @Override
        protected List<T> doInBackground(Void... params) {
            try {
                return dataProvider.getNextBlock();
            }
            catch (IOException ioe) {
                e = ioe;
                Log.e("andless.endless", "Error on query submit: " + ioe.getLocalizedMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<T> result) {
            setLoading(false);
            
            if (e == null) { // no error
            	
	            if (result == null) {
	            	if (getCount() ==  0) {
	            		
	            		// no result set for search term
	            		if (emptyHandler != null) {
	            			emptyHandler.handleEmptyResult(dataProvider.getSearchTerm());
	            		}
	            		
	            	} else {
	            		current().notifyDataSetChanged();
	            	}
	            }
	            else {
	            	addAll(result);
	            }
	            
            } else { // handle error
            	
                current().notifyDataSetChanged();
                
            	if (e != null && errorHandler != null) {
                    try { errorHandler.handleError(e); }
                    catch(Exception e) {
                        Log.e("andless.endless", "An error occured during error handling.", e);
                    }
                    e = null;
                }
            	
            }
        }
    }
}
