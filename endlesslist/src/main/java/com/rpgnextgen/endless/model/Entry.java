package com.rpgnextgen.endless.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generic entry.
 * <p>
 * Data is stored as string key/value pairs.
 * 
 * @author Mihael Schmidt
 *
 */
public class Entry {

    private Map<String, String> values = new HashMap<String, String>();
    
    /**
     * Adds key/value pair. If the passed key is already stored in this Entry the value will 
     * be replaced.
     * 
     * @param key Key
     * @param value Data value
     */
    public void add(String key, String value) {
        values.put(key, value);
    }
    
    /**
     * Returns the value associated with the passed key.
     * 
     * @param key Key (not <code>null</code>)
     * @return Data as String or <code>
     */
    public String get(String key) {
        return values.get(key);
    }
    
    public Set<String> keys() {
        return values.keySet();
    }
}
